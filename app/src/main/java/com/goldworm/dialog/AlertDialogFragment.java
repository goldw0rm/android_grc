package com.goldworm.dialog;

import com.goldworm.net.RemoteControl;
import com.goldworm.remotecontrol.R;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class AlertDialogFragment extends DialogFragment {
	
	static int systemSubType = -1; 
	
    public static AlertDialogFragment newInstance(int title, int subType) {
        AlertDialogFragment frag = new AlertDialogFragment();
        
        Bundle args = new Bundle();
        args.putInt("title", title);
        args.putInt("subType", subType);
        frag.setArguments(args);
      
        return frag;
    }

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Bundle args = getArguments();
		int title = args.getInt("title");
		systemSubType = args.getInt("subType");
		
		DialogInterface.OnClickListener positiveOnClickListener =
			new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					RemoteControl rc = RemoteControl.getInstance();
					
					if(rc != null) {
						rc.controlSystem(systemSubType);
					}
				}
			};
		
		DialogInterface.OnClickListener negativeOnClickListener =
			new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
				}
			};
				
		Resources res = getResources();
		final String yes = res.getString(R.string.yes);
		final String no = res.getString(R.string.no);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		
		builder.setTitle(title)
			   .setCancelable(true)
			   .setPositiveButton(yes, positiveOnClickListener)
			   .setNegativeButton(no, negativeOnClickListener);
		
		return builder.create();
	}
}
