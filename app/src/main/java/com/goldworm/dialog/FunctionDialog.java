package com.goldworm.dialog;

import com.goldworm.net.RemoteControl;
import com.goldworm.remotecontrol.R;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;

public class FunctionDialog extends Dialog implements OnClickListener {
	
	private final int VK_F1 = 0x70; 
	
    final int[] buttonIds = {
    	R.id.F1Button,
    	R.id.F2Button,
    	R.id.F3Button,
    	R.id.F4Button,
    	R.id.F5Button,
    	R.id.F6Button,
    	R.id.F7Button,
    	R.id.F8Button,
    	R.id.F9Button,
    	R.id.F10Button,
    	R.id.F11Button,
    	R.id.F12Button,

    	R.id.CloseButton,
    };
	
	public FunctionDialog(Context context) {
		super(context);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.function);
		this.setTitle(R.string.function);
		
		int i;
		Button button;
		int size = buttonIds.length;
		
        for(i=0; i<size; i++) {
        	button = (Button) this.findViewById(buttonIds[i]);
        	button.setOnClickListener(this);
        }
	}

	@Override
	public void onClick(View view) {
		int i;
		int keyCode = -1;
		int id = view.getId();
		
		if(id == R.id.CloseButton) {
			this.cancel();
			return;
		}
		
		for(i=0; i<12; i++) {
			if(id == buttonIds[i]) {
				keyCode = VK_F1 + i;
				break;
			}
		}
		
		RemoteControl remoteControl = RemoteControl.getInstance();
		remoteControl.controlKeyboard(keyCode);
	}
}