package com.goldworm.dialog;

import com.goldworm.remotecontrol.R;
import com.goldworm.utils.Utils;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

public class InfoDialog extends Dialog {
	
	public InfoDialog(Context context) {
		super(context);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.info);
		this.setTitle(R.string.info);
		
		Resources res = getContext().getResources();
		final String packageVersionName = Utils.getPackageVersionName(getContext());
		
		StringBuilder sb = new StringBuilder();
		sb.append(res.getString(R.string.app_name));
		sb.append(" ");
		sb.append(packageVersionName);
		sb.append("<br />Made by goldworm<br /><br />");
		sb.append("Homepage: ");
		sb.append("<a href=\"http://www.nomadconnection.com/goldworm\">Click here</a>");
		sb.append("<br /><br />");
		sb.append("Email: ");
		sb.append(res.getString(R.string.email));
		
		TextView textView = (TextView) this.findViewById(R.id.text);
		textView.setText(Html.fromHtml(sb.toString()));
		textView.setMovementMethod(LinkMovementMethod.getInstance());
	}
}
