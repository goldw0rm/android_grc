package com.goldworm.dialog;

import com.goldworm.net.RemoteControl;
import com.goldworm.remotecontrol.R;
import com.goldworm.remotecontrol.VirtualKey;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ToggleButton;

public class NumberDialog extends Dialog implements OnClickListener {
	
	private ToggleButton numPadButton;
	
    final int[] buttonIds = {
    	R.id.Num0Button,
    	R.id.Num1Button,
    	R.id.Num2Button,
    	R.id.Num3Button,
    	R.id.Num4Button,
    	R.id.Num5Button,
    	R.id.Num6Button,
    	R.id.Num7Button,
    	R.id.Num8Button,
    	R.id.Num9Button,

    	R.id.EscapeButton,
    	R.id.EnterButton,
    	R.id.CloseButton,
    	
    	R.id.PageUpButton,
    	R.id.PageDownButton,
    	R.id.SpaceButton,
    };
	
	public NumberDialog(Context context) {
		super(context);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.number);
		this.setTitle(R.string.number);
		
		int i;
		Button button;
		int size = buttonIds.length;
		
        for(i=0; i<size; i++) {
        	button = (Button) this.findViewById(buttonIds[i]);
        	button.setOnClickListener(this);
        }
        
        numPadButton = (ToggleButton) this.findViewById(R.id.NumPadButton);
	}

	@Override
	public void onClick(View view) {
		int i;
		int keyCode = -1;
		int id = view.getId();
		
		if(id == R.id.CloseButton) {
			this.cancel();
			return;
		}
		
		for(i=0; i<10; i++) {
			if(id == buttonIds[i]) {
				keyCode = numPadButton.isChecked() ? VirtualKey.VK_NUMPAD0 : VirtualKey.VK_0; 				
				keyCode += i;
				break;
			}
		}
		
		if(i == 10) {
			switch(id) {
			case R.id.EscapeButton:
				keyCode = VirtualKey.VK_ESCAPE;
				break;
			case R.id.EnterButton:
				keyCode = VirtualKey.VK_ENTER;
				break;
			case R.id.PageUpButton:
				keyCode = VirtualKey.VK_PAGEUP;
				break;
			case R.id.PageDownButton:
				keyCode = VirtualKey.VK_PAGEDOWN;
				break;
			case R.id.SpaceButton:
				keyCode = VirtualKey.VK_SPACE;
				break;
			}
		}
		
		RemoteControl remoteControl = RemoteControl.getInstance();
		if(remoteControl != null) {
			remoteControl.controlKeyboard(keyCode);
		}
	}
}
