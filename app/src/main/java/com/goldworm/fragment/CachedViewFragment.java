package com.goldworm.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class CachedViewFragment extends Fragment {

	private View cachedView;
	
	public View getCachedView() {
		return cachedView;
	}
	
	public View createCachedView(Activity activity, int layoutId) {
		LayoutInflater inflater = activity.getLayoutInflater();
		cachedView = inflater.inflate(layoutId, null);
		
		return cachedView;
	}
	
	public abstract void open(Activity activity);
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		return getCachedView();
	}
	
	@Override
	public void onDestroyView() {
		if(cachedView != null) {
			ViewGroup container = (ViewGroup) getView();
			
			if(container != null) {
				container.removeView( getCachedView() );
			}
		}
		
		super.onDestroyView();
	}

	public void close(Activity activity) {
		cachedView = null;
	}
	
	protected void sendMessage(Handler handler, int what) {
		Message msg = handler.obtainMessage(what, this);
		handler.sendMessage(msg);
	} 
}
