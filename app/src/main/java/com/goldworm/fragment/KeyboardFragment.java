package com.goldworm.fragment;

import java.util.Timer;
import java.util.TimerTask;

import com.goldworm.keyboard.WormKeyboardView;
import com.goldworm.net.RemoteControl;
import com.goldworm.net.RemoteControlThread;
import com.goldworm.remotecontrol.Constant;
import com.goldworm.remotecontrol.KeyboardPressTimerTask;
import com.goldworm.remotecontrol.MouseImageView;
import com.goldworm.remotecontrol.MouseTimerTask;
import com.goldworm.remotecontrol.R;
import com.goldworm.remotecontrol.VirtualKey;
import com.goldworm.utils.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Configuration;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TableLayout.LayoutParams;

public class KeyboardFragment extends CachedViewFragment
	implements OnCheckedChangeListener, OnTouchListener,
	OnClickListener, OnSharedPreferenceChangeListener {
	
	static final String TAG = "KEYBOARD";

	private static final int MSG_RESUME = 0;
	
	private final int MODE_KEYBOARD = 0;
	private final int MODE_MESSAGE = 1;
	private final int MODE_MOUSE = 2;
	private final int MODE_INPUT_METHOD = 3;
	private final int MODE_PREV_MODE = 4;
	
	private ToggleButton[] imeToggleButtons;

	private RemoteControlThread remoteControl;
	private final int LEFT = 0;
	
	// ClickType
	private final int UP = 0;
	private final int DOWN = 1;
	
	public static final int MSG_LONG_TOUCH = 0;
	
	// Mode
	private int currentMode = MODE_MOUSE;
	private View[] modeViews;
	private EditText messageEditText;
	private View messageLayout;
	private MouseImageView mouseView;
	private InputMethodManager imm;
	private WormKeyboardView keyboardView;
	
	private ToggleButton[] toggleButton;
	private int keyDelay;
	private int keyPeriod;
	private Timer timer;
	private TimerTask timerTask;
	private boolean isMagnifierEnabled;
	private MousePanelController mousePanelController;
	private WormKeyboardController wormKeyboardController;
	
	private static Handler handler = new Handler() {

		@Override
        public void handleMessage(Message msg) {
			
			KeyboardFragment fragment = (KeyboardFragment) msg.obj;
			
            if(msg.what == MSG_RESUME) {
            	if(fragment != null) {
            		fragment.showInputMode(msg.arg1);
            	}
            }
        }
	};
	
	private void setInputMode(final int mode) {
		if (mode == currentMode) {
			if(mode == MODE_INPUT_METHOD) {
				showInputMode(mode);
				return;
			}

			return;
		}

		hideInputMode(currentMode);
		showInputMode(mode);
	}
	
	private void hideInputMode(final int mode) {
		View view = modeViews[mode];
		
		if (currentMode == MODE_MESSAGE) {
			imm.hideSoftInputFromWindow(messageEditText.getWindowToken(), 0);
			messageEditText.clearFocus();
		} else if (currentMode == MODE_INPUT_METHOD) {
			imm.hideSoftInputFromWindow(mouseView.getWindowToken(), 0);
			mouseView.clearFocus();
		}

		if (view != null) {
			view.setVisibility(View.GONE);
		}
	}

	private void showInputMode(final int mode) {
		View view = modeViews[mode];

		if (view != null) {
			view.setVisibility(View.VISIBLE);
		}

		if (mode == MODE_MESSAGE) {
			messageEditText.requestFocus();
			// If SHOW_IMPLICIT is used, showSoftInput() doesn't work on landscape mode.
			imm.showSoftInput(messageEditText, InputMethodManager.SHOW_FORCED);
		}
		else if (mode == MODE_INPUT_METHOD) {
			// It displays a soft keyboard. DON'T FORGET to call it.
			mouseView.requestFocusFromTouch();
			// If SHOW_IMPLICIT is used, showSoftInput() doesn't work on landscape mode.
			imm.showSoftInput(mouseView, InputMethodManager.SHOW_FORCED);
		}
		
		currentMode = mode;
	}
	
	private int getMouseClickAction(int status, int clickType) {
		final int[][] action = {
			{
				RemoteControl.ACTION_LBUTTONUP,
				RemoteControl.ACTION_LBUTTONDOWN,
				RemoteControl.ACTION_LBUTTONCLICK,
			},
			
			{
				RemoteControl.ACTION_MBUTTONUP,
				RemoteControl.ACTION_MBUTTONDOWN,
				RemoteControl.ACTION_MBUTTONCLICK,
			},

			{
				RemoteControl.ACTION_RBUTTONUP,
				RemoteControl.ACTION_RBUTTONDOWN,
				RemoteControl.ACTION_RBUTTONCLICK,
			},
		};
		
		if(status < 0) {
			return -1;
		}
		
		return action[status][clickType];
	}

	/*
	private void startMagnifier() {
		if(isMagnifierEnabled) {
			remoteControl.startMagnifier();
		}
	}

	private void stopMagnifier() {
		remoteControl.stopMagnifier();
	}
	*/

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		open(activity);
	}
	
	@Override
	public void open(Activity activity) {
		
		remoteControl = RemoteControlThread.getInstance();
		
		timer = new Timer();
		timerTask = null;
		
		View view = createCachedView(activity, R.layout.keyboard);
		
		initMouseView(activity, view);
		initToggleButtons(view);
		initIMEToggleButtons(view);
		initNonRepeatableButtons(view);
		initRepeatableButtons(view);
		initInputModes(activity, view);
		initDefaultParameters(activity);
	}
	
	private void initMouseView(Activity activity, View rootView) {
		mousePanelController = new MousePanelController(activity);
		View mouseView = rootView.findViewById(R.id.MouseView);
		
		if(mouseView != null) {
			mousePanelController.setMouseView(mouseView);
			mouseView.setOnTouchListener(this);
			mouseView.setFocusable(true);
			mouseView.setFocusableInTouchMode(true);	// For IMM
		}
		else {
			Log.e(TAG, "Failed to find the mouseView.");
		}
	}
	
	private void initToggleButtons(View rootView) {
		
		final int[] buttonIds = {
			R.id.LButton,	
		};
		
		int i; 
		int size = buttonIds.length;
		toggleButton = new ToggleButton [size];
		
		for(i=0; i<size; i++) {
			toggleButton[i] = (ToggleButton) rootView.findViewById(buttonIds[i]);
			if(toggleButton[i] != null) {
				toggleButton[i].setOnCheckedChangeListener(this);
			}
		}
	}
	
	private void initIMEToggleButtons(View rootView) {
		final int[] imeButtonIds = { R.id.AltButton, R.id.CtrlButton, };
		
		final int size = imeButtonIds.length;
		imeToggleButtons = new ToggleButton[size];
		for (int i=0; i<size; i++) {
			imeToggleButtons[i] = (ToggleButton) rootView.findViewById(imeButtonIds[i]);
			imeToggleButtons[i].setOnCheckedChangeListener(this);
		}
	}
	
	private void initNonRepeatableButtons(View rootView) {
		final int[] buttonId = {
			R.id.MButton, R.id.RButton,
			R.id.KeyboardButton, R.id.MessageButton, R.id.InputMethodButton,
			R.id.SendButton,
			R.id.TabButton, R.id.EscapeButton,
		};
		
		Button button;
		int size = buttonId.length;
		for(int i=0; i<size; i++) {
			button = (Button) rootView.findViewById(buttonId[i]);
			
			if(button != null) {
				button.setOnClickListener(this);
			}
		}
	}
	
	/**
	 * Mouse wheel buttons
	 * @param rootView
	 */
	private void initRepeatableButtons(View rootView) {
		final int[] wheelButtonId = {
			// Mouse Wheel Buttons
			R.id.UpButton, R.id.DownButton,
			// Message Mode Buttons
			R.id.EnterButton, R.id.BackSpaceButton,
		};
		
		Button button;
		int size = wheelButtonId.length;
		for(int i=0; i<size; i++) {
			button = (Button) rootView.findViewById(wheelButtonId[i]);
			
			if(button != null) {
				button.setOnTouchListener(this);
			}
		}
	}
	
	private void initInputModes(Activity activity, View rootView) {
		initKeyboardMode(activity, rootView);
		initMessageMode(rootView);
		initIMEMode(activity, rootView);
		initModeViews(rootView);
	}
	
	private void initKeyboardMode(Activity activity, View rootView) {
		wormKeyboardController = new WormKeyboardController(remoteControl);
		keyboardView = (WormKeyboardView) activity.getLayoutInflater().inflate(
				R.layout.input, null);
		keyboardView.setOnKeyboardActionListener(wormKeyboardController);
		keyboardView.setKeyboard(WormKeyboardView.ENGLISH);
	
		RelativeLayout root = (RelativeLayout) rootView.findViewById(R.id.Root);
		RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		keyboardView.setVisibility(View.GONE);
		root.addView(keyboardView, layoutParams);
	}
	
	/**
	 * It should be called after calling all init?Mode(). 
	 * @param rootView
	 */
	private void initModeViews(View rootView) {
		modeViews = new View[4];
		modeViews[MODE_MOUSE] = rootView.findViewById(R.id.MouseButtons);
		modeViews[MODE_KEYBOARD] = keyboardView;
		modeViews[MODE_MESSAGE] = messageLayout;
		modeViews[MODE_INPUT_METHOD] = rootView.findViewById(R.id.IMELayout);		
	}
	
	private void initMessageMode(View rootView) {
		messageEditText = (EditText) rootView.findViewById(R.id.MessageEditText);
		messageLayout = rootView.findViewById(R.id.MessageLayout);
	}
	
	private void initIMEMode(Activity activity, View rootView) {
		imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
		mouseView = (MouseImageView) rootView.findViewById(R.id.MouseView);
	}
	
	private void initDefaultParameters(Activity activity) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
		prefs.registerOnSharedPreferenceChangeListener(this);
		keyDelay = Utils.getIntGreaterThanZeroFromPrefs(
				prefs, "key_delay", Constant.KEY_DELAY_DEFAULT);
		keyPeriod = Utils.getIntGreaterThanZeroFromPrefs(
				prefs, "key_period", Constant.KEY_PERIOD_DEFAULT);
		isMagnifierEnabled = prefs.getBoolean("magnifier", false);
	}
	
	@Override
	public void onResume() {
		super.onResume();
//		startMagnifier();
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		int keyboardMode = Utils.getIntFromPrefs(
			prefs, "keyboard_mode", Constant.KEYBOARD_MODE_DEFAULT);
		
		if(keyboardMode == MODE_PREV_MODE) { 
			keyboardMode = currentMode;
		}

		// Default IME can't be displayed if showMode() is called here.
		Message msg = handler.obtainMessage(MSG_RESUME, this);
		msg.arg1 = keyboardMode;
		handler.sendMessage(msg);
//		showInputMode(keyboardMode);
	}

	@Override
	public void onPause() {
		cancelTimerTask();
		hideInputMode(currentMode);
		
		if(toggleButton[LEFT].isChecked()) {
			toggleButton[LEFT].setChecked(false);
		}
		
//		stopMagnifier();

		super.onPause();
	}

	@Override
	/**
	 * It handles button touch only.
	 * MouseView touch is handled by MousePanelController. 
	 */
	public boolean onTouch(View v, MotionEvent event) {
		
		if( onMessageButtonTouch(v, event) )
		{
			return true;
		}
		
		if( onWheelButtonTouch(v,event) ) {
			return true;
		}
		
		if(v.getId() == R.id.MouseView) {
			setInputMode(MODE_MOUSE);
			return mousePanelController.onTouch(v, event);
		}
		
		return false;
	}
	
	public boolean onMessageButtonTouch(View v, MotionEvent event) {
		int keyCode;
		final int id = v.getId();
		
		switch(id) {
		case R.id.EnterButton:
			keyCode = VirtualKey.VK_ENTER;
			break;
		case R.id.BackSpaceButton:
			keyCode = VirtualKey.VK_BACK;
			break;
		default:
			return false;
		}

		final int action = event.getAction();
		
		if (action == MotionEvent.ACTION_DOWN) {
			if (timerTask == null) {
				int[] keyCodes = new int[1];
				keyCodes[0] = keyCode;

				remoteControl.controlKeyboard(keyCode);
				startKeyboardTimerTask(1, keyCodes);
			}
		}
		else if (action == MotionEvent.ACTION_UP) {
			cancelTimerTask();
		}

		return true;
	}
	
	private boolean onWheelButtonTouch(View v, MotionEvent event) {
		int y;
		final int id = v.getId();
		final int action = event.getAction();
		final int mouseAction = RemoteControl.ACTION_VSCROLL;
		
		switch(id) {
		case R.id.UpButton:
			y = 1;
			break;
		case R.id.DownButton:
			y = -1;
			break;
		default:
			return false;
		}

		if(action == MotionEvent.ACTION_DOWN) {
			if(timerTask == null) {
				remoteControl.controlMouse(mouseAction, 0, y);
				startMouseTimerTask(mouseAction, y);
			}
		}
		else if(action == MotionEvent.ACTION_UP) {
			cancelTimerTask();
		}
		
		return true;
	}
	
	@Override
	public void onCheckedChanged(CompoundButton button, boolean on) {
		
		final int id = button.getId();
		
		switch(id) {
		case R.id.LButton:
			int clickType = on ? DOWN : UP;
			int action = getMouseClickAction(LEFT, clickType);
			remoteControl.controlMouse(action, 0, 0);
			break;
			
		case R.id.AltButton:
			mouseView.setAltKeyOn(on);
			break;
		case R.id.CtrlButton:
			mouseView.setCtrlKeyOn(on);
			break;
		}
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
		if(key.equals("key_delay")) {
			keyDelay =
				Utils.getIntFromPrefs(prefs, key, Constant.KEY_DELAY_DEFAULT);
		}
		else if(key.equals("key_period")) {
			keyPeriod =
				Utils.getIntGreaterThanZeroFromPrefs(prefs, key, Constant.KEY_PERIOD_DEFAULT);
		}
		else if(key.equals("mousemove_threshold")) {
			int mouseMoveTreshold =
				Utils.getIntGreaterThanZeroFromPrefs(prefs, key, Constant.MOUSEMOVE_THRESHOLD_DEFAULT);
			mousePanelController.setMouseMoveTreshold(mouseMoveTreshold);
		}
		else if(key.equals("wheel_threshold")) {
			int wheelThreshold =
				Utils.getIntGreaterThanZeroFromPrefs(prefs, key, Constant.WHEEL_THRESHOLD_DEFAULT);
			mousePanelController.setWheelThreshold(wheelThreshold);
		}
		else if(key.equals("magnifier")) {
			isMagnifierEnabled = prefs.getBoolean(key, false);
		}
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();

		int action;

		switch (id) {
		case R.id.KeyboardButton:
			setInputMode(MODE_KEYBOARD);
			break;
		case R.id.MessageButton:
			setInputMode(MODE_MESSAGE);
			break;
		case R.id.InputMethodButton:
			setInputMode(MODE_INPUT_METHOD);
			break;
		case R.id.MButton:
			action = RemoteControl.ACTION_MBUTTONCLICK;
			remoteControl.controlMouse(action, 0, 0);
			break;
		case R.id.RButton:
			action = RemoteControl.ACTION_RBUTTONCLICK;
			remoteControl.controlMouse(action, 0, 0);
			break;
		case R.id.SendButton:
			Editable editable = messageEditText.getText();
			String text = editable.toString();
			remoteControl.sendText(text);
			editable.clear();
			break;
		case R.id.TabButton:
			remoteControl.controlKeyboard(VirtualKey.VK_TAB);
			break;
		case R.id.EscapeButton:
			remoteControl.controlKeyboard(VirtualKey.VK_ESCAPE);
			clearIMEToggleButtons();
			break;
		}
	}
	
	/**
	 * It handles key inputs from IME and should be called in the parent activity.
	 * @param keyCode
	 * @param event
	 * @return
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (currentMode != MODE_MOUSE) {
				setInputMode(MODE_MOUSE);
				return true;
			}
			else if(getActivity().getResources().getConfiguration().orientation ==
					Configuration.ORIENTATION_LANDSCAPE) {
				// Prevent MainActivity from entering landscape mode.
				return true;
			}
		}
		
		return false;
	}
	
	private void clearIMEToggleButtons() {
		ToggleButton button;
		int size = imeToggleButtons.length;
		for (int i = 0; i < size; i++) {
			button = imeToggleButtons[i];
			if (button.isChecked()) {
				button.setChecked(false);
			}
		}
	}

	private void startKeyboardTimerTask(final int keyCount, int[] keyCodes) {
		if(timerTask == null) {
			timerTask = new KeyboardPressTimerTask(remoteControl, keyCount, keyCodes);
			timer.scheduleAtFixedRate(timerTask, keyDelay, keyPeriod);
		}
	}
	
	private void startMouseTimerTask(final int mouseAction, final int y) {
		if(timerTask == null) {
			timerTask = new MouseTimerTask(remoteControl, mouseAction, y);
			timer.scheduleAtFixedRate(timerTask, keyDelay, keyPeriod);
		}
	}

	private void cancelTimerTask() {
		if(timerTask != null) {
			timerTask.cancel();
			timerTask = null;
		}
	}
}