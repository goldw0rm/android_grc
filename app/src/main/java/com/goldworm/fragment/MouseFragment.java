package com.goldworm.fragment;

import java.util.Timer;
import java.util.TimerTask;

import com.goldworm.net.RemoteControl;
import com.goldworm.net.RemoteControlThread;
import com.goldworm.remotecontrol.Constant;
import com.goldworm.remotecontrol.MouseTimerTask;
import com.goldworm.remotecontrol.R;
import com.goldworm.remotecontrol.VirtualKey;
import com.goldworm.utils.Utils;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.preference.PreferenceManager;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ToggleButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class MouseFragment extends CachedViewFragment
	implements OnCheckedChangeListener, OnTouchListener,
	OnClickListener, OnSharedPreferenceChangeListener {
	
	static final String TAG = "MOUSE";
	private RemoteControlThread remoteControl;
	private final int LEFT = 0;
	
	// ClickType
	private final int UP = 0;
	private final int DOWN = 1;
	
	public static final int MSG_LONG_TOUCH = 0;
	
	private ToggleButton[] toggleButton;
	private int keyDelay;
	private int keyPeriod;
	private Timer timer;
	private TimerTask timerTask;
	private boolean isMagnifierEnabled;
	private MousePanelController mousePanelController;
	
	private int getMouseClickAction(int status, int clickType) {
		final int[][] action = {
			{
				RemoteControl.ACTION_LBUTTONUP,
				RemoteControl.ACTION_LBUTTONDOWN,
				RemoteControl.ACTION_LBUTTONCLICK,
			},
			
			{
				RemoteControl.ACTION_MBUTTONUP,
				RemoteControl.ACTION_MBUTTONDOWN,
				RemoteControl.ACTION_MBUTTONCLICK,
			},

			{
				RemoteControl.ACTION_RBUTTONUP,
				RemoteControl.ACTION_RBUTTONDOWN,
				RemoteControl.ACTION_RBUTTONCLICK,
			},
		};
		
		if(status < 0) {
			return -1;
		}
		
		return action[status][clickType];
	}
	
	private void startMagnifier() {
		if(isMagnifierEnabled) {
			remoteControl.startMagnifier();
		}
	}
	
	private void stopMagnifier() {
		remoteControl.stopMagnifier();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		open(activity);
	}
	
	@Override
	public void open(Activity activity) {
		
		remoteControl = RemoteControlThread.getInstance();
		
		timer = new Timer();
		timerTask = null;
		
		View view = createCachedView(activity, R.layout.mouse);
		
		initMouseView(activity, view);
		initToggleButtons(view);
		initNonRepeatableButtons(view);
		initRepeatableButtons(view);
		initDefaultParameters(activity);
	}
	
	private void initMouseView(Activity activity, View rootView) {
		mousePanelController = new MousePanelController(activity);
		View mouseView = rootView.findViewById(R.id.MouseView);
		
		if(mouseView != null) {
			mousePanelController.setMouseView(mouseView);
			mouseView.setOnTouchListener(mousePanelController);
		}
	}
	
	private void initToggleButtons(View rootView) {
		
		final int[] buttonIds = {
			R.id.LButton,	
		};
		
		int i; 
		int size = buttonIds.length;
		toggleButton = new ToggleButton [size];
		
		for(i=0; i<size; i++) {
			toggleButton[i] = (ToggleButton) rootView.findViewById(buttonIds[i]);
			toggleButton[i].setOnCheckedChangeListener(this);
		}
	}
	
	private void initNonRepeatableButtons(View rootView) {
		final int[] buttonId = {
			R.id.MButton,
			R.id.RButton,
			R.id.BackSpaceButton,
		};
		
		Button button;
		int size = buttonId.length;
		for(int i=0; i<size; i++) {
			button = (Button) rootView.findViewById(buttonId[i]);
			
			if(button != null) {
				button.setOnClickListener(this);
			}
		}
	}
	
	/**
	 * Mouse wheel buttons
	 * @param rootView
	 */
	private void initRepeatableButtons(View rootView) {
		final int[] wheelButtonId = {
			R.id.UpButton,
			R.id.DownButton,
		};
		
		Button button;
		int size = wheelButtonId.length;
		for(int i=0; i<size; i++) {
			button = (Button) rootView.findViewById(wheelButtonId[i]);
			
			if(button != null) {
				button.setOnTouchListener(this);
			}
		}
	}
	
	private void initDefaultParameters(Activity activity) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
		prefs.registerOnSharedPreferenceChangeListener(this);
		keyDelay = Utils.getIntGreaterThanZeroFromPrefs(
				prefs, "key_delay", Constant.KEY_DELAY_DEFAULT);
		keyPeriod = Utils.getIntGreaterThanZeroFromPrefs(
				prefs, "key_period", Constant.KEY_PERIOD_DEFAULT);
		isMagnifierEnabled = prefs.getBoolean("magnifier", false);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		startMagnifier();
	}

	@Override
	public void onPause() {
		cancelTimerTask();
		
		if(toggleButton[LEFT].isChecked()) {
			toggleButton[LEFT].setChecked(false);
		}
		
		stopMagnifier();

		super.onPause();
	}

	@Override
	/**
	 * It handles button touch only.
	 * MouseView touch is handled by MousePanelController. 
	 */
	public boolean onTouch(View v, MotionEvent event) {
		int y;
		int id = v.getId();
		int action = event.getAction();
		final int mouseAction = RemoteControl.ACTION_VSCROLL;
		
		switch(id) {
		case R.id.UpButton:
			y = 1;
			break;
		case R.id.DownButton:
			y = -1;
			break;
		default:
			return false;
		}

		if(action == MotionEvent.ACTION_DOWN) {
			if(timerTask == null) {
				remoteControl.controlMouse(mouseAction, 0, y);
				startTimerTask(mouseAction, y);
			}
		}
		else if(action == MotionEvent.ACTION_UP) {
			cancelTimerTask();
		}
		
		return true;
	}
	
	@Override
	public void onCheckedChanged(CompoundButton button, boolean on) {
		int clickType = on ? DOWN : UP;
		int action = getMouseClickAction(LEFT, clickType);
		remoteControl.controlMouse(action, 0, 0);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
		if(key.equals("key_delay")) {
			keyDelay =
				Utils.getIntFromPrefs(prefs, key, Constant.KEY_DELAY_DEFAULT);
		}
		else if(key.equals("key_period")) {
			keyPeriod =
				Utils.getIntGreaterThanZeroFromPrefs(prefs, key, Constant.KEY_PERIOD_DEFAULT);
		}
		else if(key.equals("mousemove_threshold")) {
			int mouseMoveTreshold =
				Utils.getIntGreaterThanZeroFromPrefs(prefs, key, Constant.MOUSEMOVE_THRESHOLD_DEFAULT);
			mousePanelController.setMouseMoveTreshold(mouseMoveTreshold);
		}
		else if(key.equals("wheel_threshold")) {
			int wheelThreshold =
				Utils.getIntGreaterThanZeroFromPrefs(prefs, key, Constant.WHEEL_THRESHOLD_DEFAULT);
			mousePanelController.setWheelThreshold(wheelThreshold);
		}
		else if(key.equals("magnifier")) {
			isMagnifierEnabled = prefs.getBoolean(key, false);
		}
	}

	@Override
	public void onClick(View v) {
		int action;
		final int id = v.getId();
		
		switch(id) {
		case R.id.MButton:
			action = RemoteControl.ACTION_MBUTTONCLICK;
			break;
		case R.id.RButton:
			action = RemoteControl.ACTION_RBUTTONCLICK;
			break;
		case R.id.BackSpaceButton:
			remoteControl.controlKeyboard(VirtualKey.VK_BACK);
			return;
		default:
			return;
		}
		
		remoteControl.controlMouse(action, 0, 0);
	}

	
	private void startTimerTask(final int mouseAction, final int y) {
		if(timerTask == null) {
			timerTask = new MouseTimerTask(remoteControl, mouseAction, y);
			timer.scheduleAtFixedRate(timerTask, keyDelay, keyPeriod);
		}
	}

	private void cancelTimerTask() {
		if(timerTask != null) {
			timerTask.cancel();
			timerTask = null;
		}
	}
}
