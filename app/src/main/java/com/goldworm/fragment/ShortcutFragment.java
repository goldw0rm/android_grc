package com.goldworm.fragment;

import com.goldworm.net.RemoteControlThread;
import com.google.ads.*;
import com.goldworm.fragment.shortcut.EtcFragment;
import com.goldworm.fragment.shortcut.PlayerFragment;
import com.goldworm.net.RemoteControl;
import com.goldworm.remotecontrol.Constant;
import com.goldworm.remotecontrol.R;
import com.goldworm.utils.Utils;
import com.viewpagerindicator.LinePageIndicator;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Spinner;

import java.util.LinkedList;

/**
 * Shortcut page implmentation
 */
public class ShortcutFragment extends CachedViewFragment
	implements OnClickListener, OnItemSelectedListener, OnSharedPreferenceChangeListener {
	
	private static final String TAG = "SHORTCUT";
	private static final int MSG_CONNECTED = 0;
	private static final int MSG_LOAD_APP_LIST = 1;
	private static final int MSG_GET_APP_LIST_BEGIN = 2;
	private static final int MSG_GET_APP_LIST_END = 3;

	// FOCUS
	static final int FOCUS_OFF = 0;
	static final int FOCUS_ON = 1;
	static final int FOCUS_TOGGLE = 2;
	
    private MyAdapter fragmentPagerAdapter;
    private ViewPager viewPager;
	
	SharedPreferences prefs;
	private int selectedAppID = -1;
	private boolean isAppListLoaded = false;
	private boolean automaticFocus;
	private RemoteControlThread remoteControl;
	
	private ArrayAdapter<String> appAdapter;
	private Spinner appSpinner;
	private AdView adView;
	private boolean firstRun;

	private int focusedAppID;
	private LinkedList<String> appNames = new LinkedList<String> ();
	
	private Handler handler = new Handler() {
		/**
		 * Finished to detect servers.
		 */
        public void handleMessage(Message msg) {
        	
        	ShortcutFragment shortcutFragment = (ShortcutFragment) msg.obj;

			switch(msg.what) {
				case MSG_GET_APP_LIST_BEGIN:
					shortcutFragment.beginGettingAppList();
					break;

				case MSG_GET_APP_LIST_END:
					shortcutFragment.updateAppSpinner();
					break;
			}
		}
	};
	
	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if( intent.getAction().compareTo("CONNECTED") == 0 ) {
				sendMessage(MSG_GET_APP_LIST_BEGIN);
			}
		}
	};
	
	private void sendMessage(int what) {
		Message msg = handler.obtainMessage(what, this);
		handler.sendMessage(msg);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		open(activity);
	}

	@Override
	public void open(Activity activity) {
		remoteControl = RemoteControlThread.getInstance();
		prefs = PreferenceManager.getDefaultSharedPreferences(activity);
		
        IntentFilter intentFilter = new IntentFilter("CONNECTED");
        activity.registerReceiver(broadcastReceiver, intentFilter);
        
        initDefaultParameters(activity);
        
		View cachedView = createCachedView(activity, R.layout.shortcut);
		initNonRepeatableButtons(cachedView);
		initViewPager((FragmentActivity) activity, cachedView);
		initAppSpinner(activity, cachedView);
		initAdmobBanner(activity, cachedView);
		checkFirstRun(activity);		
	}
	
	void initNonRepeatableButtons(View rootView) {
        final int[] buttonIds = {
			R.id.power_button,
        };
        
        Button button;
        final int size = buttonIds.length;
        
        for(int i=0; i<size; i++) {
        	button = (Button) rootView.findViewById(buttonIds[i]);
        	
        	if(button != null) {
        		button.setOnClickListener(this);
        	}
        }
	}
	
	void initViewPager(FragmentActivity activity, View rootView)
	{
//		fragmentPagerAdapter = new MyAdapter(activity.getSupportFragmentManager(), this);
		fragmentPagerAdapter = new MyAdapter(this.getChildFragmentManager(), this);
		viewPager = (ViewPager) rootView.findViewById(R.id.shortcut_pager);
		viewPager.setAdapter(fragmentPagerAdapter);
		
        LinePageIndicator indicator = (LinePageIndicator) rootView.findViewById(R.id.indicator);
        indicator.setViewPager(viewPager);
	}
	
	void initDefaultParameters(Activity activity) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
		prefs.registerOnSharedPreferenceChangeListener(this);

		automaticFocus = prefs.getBoolean("automatic_focus", Constant.AUTOMATIC_FOCUS);
	}
	
	private void initAppSpinner(Activity activity, View rootView) {
		
        appAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item);
        appAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        
        appSpinner = (Spinner) rootView.findViewById(R.id.app_spinner);
        appSpinner.setAdapter(appAdapter);
        appSpinner.setOnItemSelectedListener(this);
	}
	
	private void initAdmobBanner(Activity activity, View rootView) {
		RelativeLayout adLayout = (RelativeLayout) rootView.findViewById(R.id.ad_layout);

		if(adLayout != null) {
			AdRequest adRequest = new AdRequest();
			adView = new AdView(activity, AdSize.BANNER, Constant.ADMOB_PUBLISHER_ID);
			adView.loadAd(adRequest);
			
			RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			adLayout.addView(adView, layoutParams);
		}
	}
    
    private void checkFirstRun(Activity activity) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
	
		String version = Utils.getPackageVersionName(activity);
		String oldVersion = prefs.getString("version", null);
		
		if(version != oldVersion) {
			SharedPreferences.Editor editor = prefs.edit();
			editor.putString("version", version);
			editor.commit();
		
			firstRun = true;
			Log.d(TAG, "version: " + version + " old: " + oldVersion);
		}
		else
		{
			firstRun = false;
		}
    }

	@Override
	public void onStart() {
		if(!isAppListLoaded) {
			sendMessage(MSG_GET_APP_LIST_BEGIN);
		}

		super.onStart();
	}

	@Override
	public void onResume() {
		super.onResume();
		
		if(firstRun) {
//			showDialog(DIALOG_FIRST_RUN);
			firstRun = false;
		}
	}

	@Override
	public void onPause() {
		super.onPause();
	}
	
	@Override
	public void onDetach() {
		close(getActivity());
		super.onDetach();
	}

	/**
	 * This should be called manually in FragmentActivity.onDestroy().
	 */
	@Override
	public void close(Activity activity) {
		if(adView != null) {
			adView.destroy();
		}
		
		activity.unregisterReceiver(broadcastReceiver);
		
		super.close(activity);
	}
	
	public int getSelectedAppID() {
		return selectedAppID;
	}

	public void beginGettingAppList() {
		if(remoteControl == null) {
			return;
		}

		new Thread(new Runnable() {
			@Override
			public void run() {
				int[] focusedAppID = new int [1];
				focusedAppID[0] = -1;

				remoteControl.getControl().getAppList(appNames, focusedAppID);
				ShortcutFragment.this.focusedAppID = focusedAppID[0];
				ShortcutFragment.this.sendMessage(MSG_GET_APP_LIST_END);
			}
		}).start();
	}

	/**
	 * Update AppSpinner with app list in main thread.
	 */
	public void updateAppSpinner() {
		for(String appName : appNames) {
			appAdapter.add(appName);
		}

		appAdapter.notifyDataSetChanged();
		appNames.clear();

		int position = 0;
		
		if(focusedAppID < 0)
		{
			String appName = prefs.getString("application", null);
			if(appName != null) {
				position = appAdapter.getPosition(appName);
			}
		}
		else
		{
			position = focusedAppID;
		}
		
		appSpinner.setSelection(position);
		isAppListLoaded = true;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		int appID = getSelectedAppID();
		
		if(appID < 0) {
			Log.e("ERROR", "appID is undefined.");
			return;
		}

		switch(id) {
		case R.id.power_button:
			remoteControl.controlShortcut(appID, RemoteControl.ACTION_POWER);
			break;
		}
	}
	
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		Log.v(TAG, "position:" + position);

		selectedAppID = position;
		
		if(automaticFocus) {
			RemoteControlThread.getInstance().controlShortcut(
					selectedAppID, FOCUS_ON, RemoteControl.ACTION_FOCUS);
		}
		
		String appName = appAdapter.getItem(position);

		SharedPreferences.Editor editor = prefs.edit();
		editor.putString("application", appName);
		editor.commit();
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		Log.v(TAG, "nothing selected.");
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
		if(key.equals("automatic_focus")) {
			automaticFocus = prefs.getBoolean(key, Constant.AUTOMATIC_FOCUS);
		}
	}
	
	/**
	 * It doesn't work in screen-off state.
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		final int appID = getSelectedAppID();
		if(appID < 0) {
			return false;
		}
		
		switch(keyCode) {
		case KeyEvent.KEYCODE_VOLUME_DOWN:
			remoteControl.controlShortcut(appID, RemoteControl.ACTION_DOWN);
			return true;
		
		case KeyEvent.KEYCODE_VOLUME_UP:
			remoteControl.controlShortcut(appID, RemoteControl.ACTION_UP);
			return true;
			
//		case KeyEvent.KEYCODE_SEARCH:
//			remoteControl.controlShortcut(appID, RemoteControl.ACTION_PLAY_AND_PAUSE);
//			return true;
		}
		
		return false;
	}
	
    public static class MyAdapter extends FragmentPagerAdapter {
    	ShortcutFragment parent;
    	
        public MyAdapter(FragmentManager fragmentManager, ShortcutFragment parent) {
            super(fragmentManager);
            this.parent = parent;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public Fragment getItem(int position) {
        	if(position == 0) {
        		PlayerFragment playerFragment = new PlayerFragment();
//        		playerFragment.setParent(parent);
        		return playerFragment;
        	}
        	else {
        		EtcFragment etcFragment = new EtcFragment();
//        		etcFragment.setParent(parent);
        		return etcFragment;
        	}
        }
    }
}