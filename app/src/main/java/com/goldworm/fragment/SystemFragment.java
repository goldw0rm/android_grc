package com.goldworm.fragment;

import com.goldworm.net.RemoteControl;
import com.goldworm.net.RemoteControlThread;
import com.goldworm.remotecontrol.R;
import com.goldworm.dialog.AlertDialogFragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.ToggleButton;

public class SystemFragment extends CachedViewFragment
	implements OnClickListener, OnSeekBarChangeListener, Handler.Callback {

	private static final int DIALOG_CONFIRM = 0;

	private static final int MSG_CONNECTED = 0;
	
	// For VolumeSeekBar
	final int PROGRESS = 0;
	final int MIN = 1;
	final int MAX = 2;
	
	final int buttonIds[] = {
		R.id.ShutdownButton,
		R.id.RebootButton,
		R.id.LogoffButton,
		R.id.LockScreenButton,
		R.id.MonitorOffButton,
		R.id.wol_button,
		R.id.SuspendButton,
		R.id.HibernateButton,
		R.id.NonSleepingButton,
	};

	private SeekBar volumeSeekBar;
	private ToggleButton nonSleepingModeButton;
	private RemoteControlThread remoteControl;
	private Handler handler;
	
	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if( intent.getAction().compareTo("CONNECTED") == 0 ) {
				SystemFragment.this.sendMessage(MSG_CONNECTED);
			}
		}
	};
	
	private void sendMessage(final int what) {
		Message msg = handler.obtainMessage(what, this);
		handler.sendMessage(msg);
	}
	
	private void loadCurrnetVolumeStatus() {
		remoteControl.getVolume(handler);
	}
	
	private void setVolumeSeekBar(final boolean enabled, final int[] volume) {
		if(enabled && volume != null) {
			volumeSeekBar.setMax(volume[MAX]);
			volumeSeekBar.setProgress(volume[PROGRESS]);
			volumeSeekBar.setOnSeekBarChangeListener(this);			
		}
	}
	
//	private void turnOffNonSleepingMode() {
//		remoteControl.setNonSleepingMode(0);
//		nonSleepingModeButton.setChecked(false);
//	}
	
	@Override
	public void open(Activity activity) {
		handler = new Handler(this);
		remoteControl = RemoteControlThread.getInstance();
		
        IntentFilter intentFilter = new IntentFilter("CONNECTED");
        activity.registerReceiver(broadcastReceiver, intentFilter);
        
        View cachedView = createCachedView(activity, R.layout.system);
        initViews(cachedView);
	}
	
	private void initViews(View rootView) {
		int i;
		Button button;
		int length = buttonIds.length;
		for(i=0; i<length; i++) {
			button = (Button) rootView.findViewById(buttonIds[i]);
			
			if(button != null) {
				button.setOnClickListener(this);
			}
		}
		
		volumeSeekBar = (SeekBar) rootView.findViewById(R.id.VolumeSeekBar);
		nonSleepingModeButton = (ToggleButton) rootView.findViewById(R.id.NonSleepingButton);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		open(activity);
	}
	
	@Override
	public void onResume() {
		loadCurrnetVolumeStatus();		
		super.onResume();
	}
	
	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}

	@Override
	public void onDetach() {
		close(getActivity());
		super.onDetach();
	}

	@Override
	public void close(Activity activity) {
		activity.unregisterReceiver(broadcastReceiver);
		super.close(activity);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		int subType = -1;
		
		switch(id) {
		case R.id.ShutdownButton:
			subType = RemoteControl.SYSTEM_SHUTDOWN;
			break;
		case R.id.RebootButton:
			subType = RemoteControl.SYSTEM_REBOOT;
			break;
		case R.id.LogoffButton:
			subType = RemoteControl.SYSTEM_LOGOFF;
			break;
		case R.id.MonitorOffButton:
			subType = RemoteControl.SYSTEM_MONITOR;
			break;
		case R.id.LockScreenButton:
			subType = RemoteControl.SYSTEM_LOCK_SCREEN;
			break;
		case R.id.wol_button:
			remoteControl.wakeOnLan();
			return;
		case R.id.SuspendButton:
			subType = RemoteControl.SYSTEM_SUSPEND;
			break;
		case R.id.HibernateButton:
			subType = RemoteControl.SYSTEM_HIBERNATE;
			break;
		case R.id.NonSleepingButton:
			final boolean on = nonSleepingModeButton.isChecked();
			remoteControl.setNonSleepingMode(handler, on ? 1 : 0);
			break;
		}
		
		if(subType > -1) {
			showDialog(DIALOG_CONFIRM, subType);
		}
	}
	
	void showDialog(int dialogId, int subType) {
		AlertDialogFragment dialog =
			AlertDialogFragment.newInstance(R.string.confirm_system_command, subType);
		
		dialog.show( getActivity().getSupportFragmentManager(), "alertDialog");
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
	}

	@Override
	public void onStartTrackingTouch(SeekBar arg0) {
		
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		int progress = seekBar.getProgress();
		remoteControl.setVolume(progress);
	}

	@Override
	public boolean handleMessage(Message msg) {
    	switch(msg.what) {
    	case MSG_CONNECTED:
    		/**
    		 * Finished to detect servers.
    		 */
    		loadCurrnetVolumeStatus();
//    		turnOffNonSleepingMode();
    		break;
    		
    	case RemoteControlThread.NON_SLEEPING_MODE:
    		return handleNonSleepingMode(msg);
    		
    	case RemoteControlThread.GET_VOLUME:
    		return handleGetVolume(msg);
    	}
    	
		return false;
	}

	private boolean handleGetVolume(Message msg) {
		final boolean success = (msg.arg1 == 0) ? false : true;
		final int[] volume = (int[]) msg.obj;
		
		setVolumeSeekBar(success, volume);
		
		return true;
	}

	private boolean handleNonSleepingMode(Message msg) {
		final boolean success = (msg.arg1 == 0) ? false : true;
		final boolean on = (msg.arg2 == 0) ? false : true; 
		
		if(!success) {
			nonSleepingModeButton.setChecked(!on);
		}
		
		return true;
	}
}
