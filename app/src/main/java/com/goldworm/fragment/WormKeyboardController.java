package com.goldworm.fragment;

import android.inputmethodservice.KeyboardView.OnKeyboardActionListener;
import android.util.Log;

import com.goldworm.net.RemoteControlThread;

public class WormKeyboardController implements OnKeyboardActionListener {
	
	private static final String TAG = "KEYBOARD";
	
	private RemoteControlThread remoteControl;
	
	public WormKeyboardController(RemoteControlThread remoteControl) {
		this.remoteControl = remoteControl;
	}

	@Override
	public void onKey(int count, int[] keyCodes) {
		Log.d(TAG, "onKey: " + count);

		if (count > 0) {
			remoteControl.controlKeyboard(count, keyCodes);
		}
	}

	@Override
	public void onPress(int keyCode) {
		Log.d(TAG, "onPress: " + keyCode);
	}

	@Override
	public void onRelease(int keyCode) {
		Log.d(TAG, "onRelease: " + keyCode);
	}

	@Override
	public void onText(CharSequence text) {
		Log.d(TAG, "onText: " + text);
	}

	@Override
	public void swipeDown() {
	}

	@Override
	public void swipeLeft() {
	}

	@Override
	public void swipeRight() {
	}

	@Override
	public void swipeUp() {
	}
}
