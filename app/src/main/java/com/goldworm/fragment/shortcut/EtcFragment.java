package com.goldworm.fragment.shortcut;

import com.goldworm.fragment.ShortcutFragment;
import com.goldworm.net.RemoteControl;
import com.goldworm.net.RemoteControlThread;
import com.goldworm.remotecontrol.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class EtcFragment extends Fragment implements OnClickListener {

	private RemoteControlThread remoteControl;
	
	private int getSelectedAppID() {
		ShortcutFragment parent = (ShortcutFragment) this.getParentFragment();

		if(parent == null) {
			Log.e("ERROR", "parent is null");
		}

		return parent.getSelectedAppID();
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		remoteControl = RemoteControlThread.getInstance();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.etc_in_shortcut, container, false);
        
        initNonRepeatableButtons(v);

        return v;
	}
	
	void initNonRepeatableButtons(View rootView) {
        final int[] buttonIds = {
			R.id.clover_button,
			R.id.heart_button,
			R.id.diamond_button,
			R.id.spade_button,
			
			// subtitle
			R.id.subtitle_backward_button,
			R.id.subtitle_reset_button,
			R.id.subtitle_button,
			R.id.subtitle_forward_button,

			// audio
			R.id.audio_backward_button,
			R.id.audio_reset_button,
			R.id.audio_change_button,
			R.id.audio_forward_button,
			
			// playstation
			R.id.circle_button,
			R.id.cross_button,
			R.id.rectangle_button,
			R.id.triangle_button,
        };
        
        Button button;
        final int size = buttonIds.length;
        
        for(int i=0; i<size; i++) {
        	button = (Button) rootView.findViewById(buttonIds[i]);
        	
        	if(button != null) {
        		button.setOnClickListener(this);
        	}
        }
	}

	@Override
	public void onClick(View v) {
		final int id = v.getId();
		final int appID = getSelectedAppID();
		
		if(appID < 0) {
			Log.e("ERROR", "appID is undefined.");
			return;
		}

		switch(id) {
		// etc
		case R.id.clover_button:
			controlShortcut(appID, RemoteControl.ACTION_CLOVER);
			break;
		case R.id.heart_button:
			controlShortcut(appID, RemoteControl.ACTION_HEART);
			break;
		case R.id.diamond_button:
			controlShortcut(appID, RemoteControl.ACTION_DIAMOND);
			break;
		case R.id.spade_button:
			controlShortcut(appID, RemoteControl.ACTION_SPADE);
			break;
			
		// subtitle
		case R.id.subtitle_backward_button:
			controlShortcut(appID, RemoteControl.ACTION_SUBTITLE_BACKWARD);
			break;
		case R.id.subtitle_reset_button:
			controlShortcut(appID, RemoteControl.ACTION_SUBTITLE_RESET);
			break;
		case R.id.subtitle_button:
			controlShortcut(appID, RemoteControl.ACTION_SUBTITLE);
			break;
		case R.id.subtitle_forward_button:
			controlShortcut(appID, RemoteControl.ACTION_SUBTITLE_FORWARD);
			break;

		// audio
		case R.id.audio_backward_button:
			controlShortcut(appID, RemoteControl.ACTION_AUDIO_BACKWARD);
			break;
		case R.id.audio_reset_button:
			controlShortcut(appID, RemoteControl.ACTION_AUDIO_RESET);
			break;
		case R.id.audio_change_button:
			controlShortcut(appID, RemoteControl.ACTION_AUDIO_CHANGE);
			break;
		case R.id.audio_forward_button:
			controlShortcut(appID, RemoteControl.ACTION_AUDIO_FORWARD);
			break;
			
		// playstation
		case R.id.circle_button:
			controlShortcut(appID, RemoteControl.ACTION_CIRCLE);
			break;
		case R.id.cross_button:
			controlShortcut(appID, RemoteControl.ACTION_CROSS);
			break;
		case R.id.rectangle_button:
			controlShortcut(appID, RemoteControl.ACTION_RECTANGLE);
			break;
		case R.id.triangle_button:
			controlShortcut(appID, RemoteControl.ACTION_TRIANGLE);
			break;
		}
	}
	
	private void controlShortcut(final int appID, final int action) {
		final int keyStatus = 0;
		remoteControl.controlShortcut(appID, keyStatus, action);
	}
}
