package com.goldworm.fragment.shortcut;

import java.util.Timer;
import java.util.TimerTask;

import com.goldworm.dialog.NumberDialog;
import com.goldworm.fragment.ShortcutFragment;
import com.goldworm.net.RemoteControl;
import com.goldworm.net.RemoteControlThread;
import com.goldworm.remotecontrol.Constant;
import com.goldworm.remotecontrol.FileBrowserActivity;
import com.goldworm.remotecontrol.R;
import com.goldworm.utils.Utils;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

/**
 * The sub fragment for a player in shortcut fragment 
 * @author goldworm
 *
 */
public class PlayerFragment extends Fragment
	implements OnClickListener, OnTouchListener, OnSharedPreferenceChangeListener, Handler.Callback {
	
	private static final String TAG = "PLAYER_IN_SHORTCUT";
	
	// FOCUS
	static final int FOCUS_OFF = 0;
	static final int FOCUS_ON = 1;
	static final int FOCUS_TOGGLE = 2;
	
    final int[] arrowButtonIds = {
    	R.id.left_button,
    	R.id.right_button,
    	R.id.up_button,
    	R.id.down_button,    		
    };
    
    private Handler handler;
	private NumberDialog numberDialog;
	private int keyDelay;
	private int keyPeriod;
	private Timer timer;
	private TimerTask timerTask;
	private RemoteControlThread remoteControl;
	
	private int getSelectedAppID() {
		ShortcutFragment parent = (ShortcutFragment) this.getParentFragment();

		if(parent == null) {
			Log.e("ERROR", "parent is null");
		}
		
		return parent.getSelectedAppID();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		open(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	public void open(Activity activity) {
		timer = new Timer();
		timerTask = null;
		
		remoteControl = RemoteControlThread.getInstance();
		
        initDefaultParameters(activity);
        
        numberDialog = new NumberDialog(activity);
        handler = new Handler(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.player_in_shortcut, container, false);

		initNonRepeatableButtons(v);
		initArrowButtons(v);
        
        return v;
	}

	@Override
	public void onPause() {
		cancelTimerTask();
		super.onPause();
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	void initDefaultParameters(Activity activity) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
		prefs.registerOnSharedPreferenceChangeListener(this);

		keyDelay = Utils.getIntGreaterThanZeroFromPrefs(prefs, "key_delay", Constant.KEY_DELAY_DEFAULT);
		keyPeriod = Utils.getIntGreaterThanZeroFromPrefs(prefs, "key_period", Constant.KEY_PERIOD_DEFAULT);
	}
	
	void initNonRepeatableButtons(View rootView) {
        final int[] buttonIds = {
			R.id.mute_button,
			R.id.next_button,
			R.id.prev_button,
			R.id.play_button,
			R.id.stop_button,
			R.id.full_button,
			R.id.focus_button,
			R.id.title_button,
			R.id.number_button,
			R.id.open_button,
        };
        
        Button button;
        final int size = buttonIds.length;
        
        for(int i=0; i<size; i++) {
        	button = (Button) rootView.findViewById(buttonIds[i]);
        	
        	if(button != null) {
        		button.setOnClickListener(this);
        	}
        }
	}
	
	private void initArrowButtons(View rootView) {
		Button button;
        int size = arrowButtonIds.length;

        for(int i=0; i<size; i++) {
        	button = (Button) rootView.findViewById(arrowButtonIds[i]);
        	
        	if(button != null) {
        		button.setOnTouchListener(this);
        	}
        }
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
		if(key.equals("key_delay")) {
			keyDelay =
				Utils.getIntGreaterThanZeroFromPrefs(prefs, key, Constant.KEY_DELAY_DEFAULT);
		}
		else if(key.equals("key_period")) {
			keyPeriod =
				Utils.getIntGreaterThanZeroFromPrefs(prefs, key, Constant.KEY_PERIOD_DEFAULT);
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		
		final int selectedAppID = getSelectedAppID();
		
		Log.v(TAG, "onTouch()" + event.toString());
		
		if(selectedAppID < 0) {
			Log.e(TAG, "AppID is undefined. in ShortCutActivity.onTouch()");
			return false;
		}
		
		int id = v.getId();
		int action = event.getAction();
		int shortcut = -1;
		
		switch(id) {
		case R.id.left_button:
			shortcut = RemoteControl.ACTION_LEFT;
			break;
		case R.id.right_button:
			shortcut = RemoteControl.ACTION_RIGHT;
			break;
		case R.id.up_button:
			shortcut = RemoteControl.ACTION_UP;
			break;
		case R.id.down_button:
			shortcut = RemoteControl.ACTION_DOWN;
			break;
		default:
			return false;
		}

		if(action == MotionEvent.ACTION_DOWN) {
			if(timerTask == null) {
				controlShortcut(selectedAppID, Constant.KEY_CLICK, shortcut);
				startTimerTask(shortcut);
			}
		}
		else if(action == MotionEvent.ACTION_UP) {
			cancelTimerTask();
		}
		
		return false;
	}

	@Override
	public void onClick(View v) {
		final int id = v.getId();
		final int appID = getSelectedAppID();
		Activity activity = getActivity();
		
		if(appID < 0) {
			Log.e("ERROR", "appID is undefined.");
			return;
		}

		switch(id) {
		case R.id.DownButton:
			controlShortcut(appID, RemoteControl.ACTION_DOWN);
			break;
		case R.id.UpButton:
			controlShortcut(appID, RemoteControl.ACTION_UP);
			break;
		case R.id.next_button:
			controlShortcut(appID, RemoteControl.ACTION_NEXT);
			break;
		case R.id.prev_button:
			controlShortcut(appID, RemoteControl.ACTION_PREV);
			break;
		case R.id.play_button:
			controlShortcut(appID, RemoteControl.ACTION_PLAY_AND_PAUSE);
			break;
		case R.id.full_button:
			controlShortcut(appID, RemoteControl.ACTION_FULL_SCREEN);
			break;
		case R.id.mute_button:
			controlShortcut(appID, RemoteControl.ACTION_MUTE);
			break;
		case R.id.left_button:
			controlShortcut(appID, RemoteControl.ACTION_LEFT);
			break;
		case R.id.right_button:
			controlShortcut(appID, RemoteControl.ACTION_RIGHT);
			break;
		case R.id.stop_button:
			controlShortcut(appID, RemoteControl.ACTION_STOP);
			break;
			
		case R.id.focus_button:
			controlShortcut(appID, FOCUS_TOGGLE, RemoteControl.ACTION_SHOW_HIDE);
			break;
		case R.id.title_button:
			remoteControl.getTitle(handler, appID);
			break;
		case R.id.number_button:
			numberDialog.setOwnerActivity(activity);
			numberDialog.show();
			break;
		case R.id.open_button:
			remoteControl.cwd(handler);
			break;
		}
	}
	
	private void controlShortcut(final int appID, final int action) {
		controlShortcut(appID, 0, action);
	}
	
	private void controlShortcut(final int appID, final int keyStatus, final int action) {
		remoteControl.controlShortcut(appID, keyStatus, action);
	}
	
	private class ShortcutPressTimerTask extends TimerTask {
		private int appID;
		private int shortcut;
		
		public ShortcutPressTimerTask(int appID, int shortcut) {
			super();

			this.appID = appID;
			this.shortcut = shortcut;
		}
		
		@Override
		public void run() {
			PlayerFragment.this.controlShortcut(appID, Constant.KEY_CLICK, shortcut);
		}
	}
	
	private void startTimerTask(final int shortcut) {
		if(timerTask == null) {
			timerTask = new ShortcutPressTimerTask(getSelectedAppID(), shortcut);
			timer.scheduleAtFixedRate(timerTask, keyDelay, keyPeriod);
		}
	}

	private void cancelTimerTask() {
		if(timerTask != null) {
			timerTask.cancel();
			timerTask = null;
		}
	}

	@Override
	public boolean handleMessage(Message msg) {
		
		switch(msg.what) {
		case RemoteControlThread.TITLE:
			return handleGetTitle(msg);
			
		case RemoteControlThread.CWD:
			return handleCwd(msg);
		}
		
		return false;
	}

	private boolean handleCwd(Message msg) {
		final String cwd = (String) msg.obj;
		if(cwd == null) {
			return false;
		}
		
		final int appID = this.getSelectedAppID();
		
		Intent intent = new Intent(getActivity(), FileBrowserActivity.class);
		intent.putExtra("cwd", cwd);
		intent.putExtra("appID", appID);
		
		startActivity(intent);

		return true;
	}

	private boolean handleGetTitle(Message msg) {
		final String title = (String) msg.obj;
		if(title == null) {
			return false;
		}
		
		Toast toast = Toast.makeText(getActivity(), title, Toast.LENGTH_SHORT);
		toast.show();

		return true;
	}
}