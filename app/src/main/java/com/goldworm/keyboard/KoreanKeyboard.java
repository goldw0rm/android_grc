package com.goldworm.keyboard;

import java.util.List;
import android.content.Context;
import android.util.SparseArray;

public class KoreanKeyboard extends WormKeyboard {

	private static final char[][] hangul =	{
		{ 'ㅂ', 'ㅃ' },
		{ 'ㅈ', 'ㅉ' },
		{ 'ㄷ', 'ㄸ' },
		{ 'ㄱ', 'ㄲ' },
		{ 'ㅅ', 'ㅆ' },
		{ 'ㅐ', 'ㅒ' },
		{ 'ㅔ', 'ㅖ' },
		{ ',', '<' },
		{ '.', '>' },
		{ '/', '?' },
	};

	SparseArray<char[]> hangulCodeMap;
	
	public KoreanKeyboard(Context context, int xmlLayoutResId) {
		super(context, xmlLayoutResId, WormKeyboard.KOREAN);

		final int[] keyCode = {
			0x51, 0x57, 0x45, 0x52, 0x54, 0x4F, 0x50, 0xBC, 0xBE, 0xBF
		};
		
		int length = hangul.length;
		hangulCodeMap = new SparseArray<char[]> (length * 2);
		
		for(int i=0; i<length; i++) {
			hangulCodeMap.put(keyCode[i], hangul[i]);
		}
	}

	public void handleShift() {
		List<Key> keys = getKeys();
		
		int i;
		Key key;
		int size = keys.size();
		
		int on = stickyKeys[SHIFT].on ? 1 : 0;
		
		for(i=0; i<size; i++) {
			key = keys.get(i);
			
			if(key.label != null && key.label.length() == 1)
			{
				char[] hangul = hangulCodeMap.get(key.codes[0]);

				if(hangul != null) {
					key.label = Character.toString(hangul[on]);
				}
			}
		}
	}
}
