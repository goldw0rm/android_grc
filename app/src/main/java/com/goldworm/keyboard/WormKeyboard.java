package com.goldworm.keyboard;

import java.util.List;

import com.goldworm.remotecontrol.VirtualKey;
import com.goldworm.utils.Flags;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.inputmethodservice.Keyboard;

public class WormKeyboard extends Keyboard {
	
	public static final int ALT = 0;
	public static final int CTRL = 1;
	public static final int SHIFT = 2;
	
    private static final int[] stickyKeyCodes = {
    	VirtualKey.VK_ALT,
    	VirtualKey.VK_CTRL,
    	VirtualKey.VK_SHIFT,
    };
    
    // From 33 in ASCII Code.
    private static final char[] shiftedHangulCode = {
    	'1',	// ! 33
    	'\'',	// " 34
    	'3',	// # 35
    	'4',	// $ 36
    	'5',	// % 37
    	'7',	// & 38
    	'"',	// ' 39
    	'9',	// ( 40
    	'0',	// ) 41
    	'8',	// * 42
    	'=',	// + 43
    	'<',	// , 44
    	'_',	// - 45
    	'>',	// . 46
    	'?',	// / 47
    	')',	// 0 48
    	'!',	// 1 49
    	'@',	// 2 50
    	'#',	// 3 51
    	'$',	// 4 52
    	'%', 	// 5 53
    	'^', 	// 6 54
    	'&', 	// 7 55
    	'*', 	// 8 56
    	'(',	// 9 57
    	';',	// : 58
    	':',	// ; 59
    	',',	// < 60
    	'+', 	// = 61
    	'.', 	// > 62
    	'/',	// ? 63
    	'2',	// @ 64
    	'a',	// A 65
    	'b',	// B 66
    	'c',	// C 67
    	'd',	// D 68
    	'e', 	// E 69
    	'f',	// F 70
    	'g',	// G 71
    	'h',	// H 72
    	'i',	// I 73
    	'j',	// J 74
    	'k',	// K 75
    	'l', 	// L 76
    	'm',	// M 77
    	'n',	// N 78
    	'o',	// O 79
    	'p',	// P 80
    	'q',	// Q 81
    	'r',	// R 82
    	's',	// S 83
    	't',	// T 84
    	'u',	// U 85
    	'v',	// V 86
    	'w',	// W 87
    	'x',	// X 88
    	'y',	// y 89
    	'z',	// Z 90
    	'{',	// [ 91
    	'|',	// \ 92
    	'}',	// ] 93
    	'6',	// ^ 94
    	'-',	// _ 95
    	'~',	// ` 96
    	'A',	// a 97
    	'B',	// b 98
    	'C',	// c 99
    	'D',	// D 100
    	'E',	// e 101
    	'F',	// f 102
    	'G',	// g 103
    	'H',	// h 104
    	'I',	// i 105
    	'J',	// j 106
    	'K',	// k 107
    	'L',	// l 108
    	'M',	// m 109
    	'N',	// n 110
    	'O',	// o 111
    	'P',	// p 112
    	'Q',	// q 113
    	'R',	// r 114
    	'S',	// s 115
    	'T',	// t 116
    	'U',	// u 117
    	'V',	// v 118
    	'W',	// w 119
    	'X',	// x 120
    	'Y', 	// y 121
    	'Z',	// z 122
    	'[',	// { 123
    	'\\',	// | 124
    	']',	// } 125
    	'`',	// ~ 126
    };

	protected Flags stickyBits;
	protected Key[] stickyKeys;
	
	public static final int ENGLISH = 0;
	public static final int NUMBER = 1;
	public static final int FUNCTION = 2;
	public static final int SPECIAL = 3;
	public static final int KOREAN = 4;
	private int keyboardType;

	public WormKeyboard(Context context, int xmlLayoutResId, int keyboardType) {
		super(context, xmlLayoutResId);
		this.keyboardType = keyboardType;
	}
	
    @Override
    protected Key createKeyFromXml(Resources res, Row parent, int x, int y, 
            XmlResourceParser parser) {
        Key key = new Key(res, parent, x, y, parser);
        
        int i;
        int length = stickyKeyCodes.length;
        
        if(stickyKeys == null) {
    		stickyKeys = new Key [3];
        }
        
        for(i=0; i<length; i++) {
	        if(key.codes[0] == stickyKeyCodes[i]) {
	        	stickyKeys[i] = key;
	        }
        }

        return key;
    }

	public boolean isKeyOn(int keyCode) {
		Key key;
		
		int length = stickyKeyCodes.length;
		
		for(int i=0; i<length; i++) {
			key = stickyKeys[i];
			
			if(key != null && key.codes[0] == keyCode) {
				return key.on;
			}
		}
		
		return false;
	}
	
	public void clearStickyKeys() {
		Key key;
		
		int length = stickyKeyCodes.length;
		
		for(int i=0; i<length; i++) {
			key = stickyKeys[i];
			key.on = false;
		}		
	}
	
	public void handleShift() {
		if(keyboardType != ENGLISH && keyboardType != NUMBER) {
			return;
		}
		
		List<Key> keys = getKeys();
		
		int i;
		Key key;
		int size = keys.size();
		
		for(i=0; i<size; i++) {
			key = keys.get(i);
			
			if(key.label != null && key.label.length() == 1)
			{
				int index = key.label.charAt(0) - 33;
				int length = shiftedHangulCode.length;
				
				if(index >= 0 && index < length) {
					char ch = shiftedHangulCode[index];
					key.label = Character.toString(ch);
				}
			}
		}
	}
	
//	public void handleShift() {
//		List<Key> keys = getKeys();
//		
//		int i;
//		Key key;
//		char ch;
//		int size = keys.size();
//		boolean isShifted = stickyKeys[SHIFT].on;
//		int offset = isShifted ? -0x20 : 0x20;
//		
//		for(i=0; i<size; i++) {
//			key = keys.get(i);
//			
//			if(key.label != null && key.label.length() == 1)
//			{
//				ch = key.label.charAt(0);
//				
//				if(Character.isLetter(ch)) {
//					ch += offset;
//					key.label = Character.toString(ch);
//				}
//			}
//		}
//	}
}
