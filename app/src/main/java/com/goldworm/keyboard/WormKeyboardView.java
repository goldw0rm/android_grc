package com.goldworm.keyboard;

import java.util.Locale;

import com.goldworm.remotecontrol.R;
import com.goldworm.remotecontrol.VirtualKey;

import android.content.Context;
import android.inputmethodservice.KeyboardView;
import android.util.AttributeSet;
import android.util.Log;

public class WormKeyboardView extends KeyboardView {
	
	private static final String TAG = "KEYBOARD";
	
	public static final int ENGLISH = 0;
	public static final int NUMBER = 1;
	public static final int FUNCTION = 2;
	public static final int SPECIAL = 3;
	public static final int KOREAN = 4;
	
	public static final int ALT = 0x01;
	public static final int CTRL = 0x02;
	public static final int SHIFT = 0x03;
	
	private WormKeyboard currentKeyboard;
	private WormKeyboard[] keyboards;
	private OnKeyboardActionListener externalListener;
	private OnKeyboardActionListener internalListener;
	
	public WormKeyboardView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	
	public WormKeyboardView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	private void init(Context context) {
		int keyboardResId[] = {
			R.xml.english,
			R.xml.number,
			R.xml.function,
			R.xml.special,
			R.xml.korean,
		};
		
		int i;
		int length = keyboardResId.length;
		keyboards = new WormKeyboard [length];
		
		int size = length - 1;
		for(i=0; i<size; i++) {
			keyboards[i] = new WormKeyboard(context, keyboardResId[i], i);
		}
		keyboards[KOREAN] = new KoreanKeyboard(context, keyboardResId[i]);
		
		internalListener = new KeyboardActionListener();
		super.setOnKeyboardActionListener(internalListener);
	}
	
	public void setKeyboard(int index) {
		
		if(index == KOREAN) {
			Locale[] locales = Locale.getAvailableLocales();
			
			// Maybe this phone doesn't support korean keyboard.
			if(locales == null || locales.length == 0) {
				return;
			}
			
			int i;
			Locale locale;
			int length = locales.length;

			for(i=0; i<length; i++) {
				locale = locales[i];
				
				if( locale.getLanguage().equalsIgnoreCase("ko") ) {
					break;
				}
			}
			
			if(i == length) {
				// This phone doesn't have korean locale.
				return;
			}
		}
		
		currentKeyboard = keyboards[index]; 
		this.setKeyboard(currentKeyboard);
	}
	
	public boolean isKeyOn(int keyCode) {
		WormKeyboard keyboard = (WormKeyboard) this.getKeyboard();
		return keyboard.isKeyOn(keyCode);
	}
	
	@Override
	public void setOnKeyboardActionListener(OnKeyboardActionListener listener) {
		externalListener = listener;
	}
	
	class KeyboardActionListener
		implements KeyboardView.OnKeyboardActionListener {

		private int[] keyCodes;
		
	    final int[] stickyKeyCodes = {
	        	VirtualKey.VK_ALT,
	        	VirtualKey.VK_CTRL,
	        	VirtualKey.VK_SHIFT,
	        };  
		
		public KeyboardActionListener() {
			super();
			keyCodes = new int [4];
		}

		@Override
		public void onKey(int primaryCode, int[] keyCodes) {
			Log.d(TAG, "onKey: " + primaryCode);
			
			if(primaryCode > 0xFF) {
				return;
			}

			switch(primaryCode) {
			case VirtualKey.VK_ALT:
				return;
			case VirtualKey.VK_CTRL:
				return;
			case VirtualKey.VK_SHIFT:
				currentKeyboard.handleShift();
				WormKeyboardView.this.invalidateAllKeys();
				return;
			}
			
			boolean on;
			int keyCode;
			int count = 0;
			int length = stickyKeyCodes.length;
			
			for(int i=0; i<length; i++) {
				keyCode = stickyKeyCodes[i];
				if(keyCode == primaryCode) {
					// Ignore Alt, Ctrl and Shift.
					return;
				}
				
				on = WormKeyboardView.this.isKeyOn(keyCode);
				
				if(on) {
					this.keyCodes[count] = keyCode;
					count++;
				}
			}
			
			this.keyCodes[count] = primaryCode;
			count++;

			externalListener.onKey(count, this.keyCodes);
		}

		@Override
		public void onPress(int keyCode) {
			Log.d(TAG, "onPress: " + keyCode);
			externalListener.onPress(keyCode);
		}

		@Override
		public void onRelease(int keyCode) {
			Log.d(TAG, "onRelease: " + keyCode);

			// Change keyboard type.
			if(keyCode > 0xFF) {
				int index = keyCode - 0x100;
				setKeyboard(index);
			}
			else {
				externalListener.onRelease(keyCode);
			}
		}

		@Override
		public void onText(CharSequence text) {
			externalListener.onText(text);
		}

		@Override
		public void swipeDown() {
			externalListener.swipeDown();
		}

		@Override
		public void swipeLeft() {
			externalListener.swipeLeft();
		}

		@Override
		public void swipeRight() {
			externalListener.swipeRight();
		}

		@Override
		public void swipeUp() {
			externalListener.swipeUp();
		}
	}
}
