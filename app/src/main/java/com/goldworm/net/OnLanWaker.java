package com.goldworm.net;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

import com.goldworm.utils.Utils;

public final class OnLanWaker {
	
	private static final byte[] WOL_PREFIX =
		{ (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF };
	private static byte[] BROADCAST_IP = { (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF };
	// Any port is ok in WOL.
	private final static int PORT = 7;
	private final static int WOL_PACKET_SIZE = 6 + 6 * 16;
	
	public static boolean run(final long mac) {
		final byte[] macAddress = Utils.convertMacAddress(mac);
		return run(macAddress);
	}
	
	public static boolean run(final byte[] macAddress) {
		final ByteBuffer byteBuffer = ByteBuffer.allocate(WOL_PACKET_SIZE);
		
		byteBuffer.put(WOL_PREFIX);
		for(int i=0; i<16; i++) {
			byteBuffer.put(macAddress);
		}
		
		try {
			final InetAddress address = InetAddress.getByAddress(BROADCAST_IP);
			final DatagramPacket packet = new DatagramPacket(
					byteBuffer.array(), WOL_PACKET_SIZE, address, PORT);

			final DatagramSocket socket = new DatagramSocket();
			socket.setBroadcast(true);
			socket.send(packet);
			socket.close();
			
			return true;

		} catch (UnknownHostException uhe) {
			uhe.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return false;
	}
}
