package com.goldworm.net;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.LinkedList;
import java.util.List;

import com.goldworm.remotecontrol.Constant;
import com.goldworm.remotecontrol.FileInfo;
import com.goldworm.remotecontrol.R;
import com.goldworm.remotecontrol.Result;
import com.goldworm.remotecontrol.protocol.MessageCreator;
import com.goldworm.utils.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.ArrayAdapter;

public class RemoteControl /* implements Runnable */{

	private static final String TAG = "REMOTE_CONTROL";

	// COMMAND TYPE
	public static final int CMD_GET_APP_LIST = 0;
	public static final int CMD_SET_APP = 1;
	public static final int CMD_SHORTCUT = 2;
	public static final int CMD_SYSTEM_CONTROL = 3;
	public static final int CMD_SERVER = 4;
	public static final int CMD_MOUSE = 5;
	public static final int CMD_KEYBOARD = 6;
	public static final int CMD_TEXT = 7;
	public static final int CMD_FILE_BROWSE = 8;
	public static final int CMD_EXIT = -1;

	// SERVER COMMAND SUBTYPE
	public static final int SERVER_AUTHENTICATE = 0;
	public static final int SERVER_DETECTION = 1;

	// SHORTCUT COMMAND SUBTYPE
	public static final int SHORTCUT_KEY = 0;
	public static final int SHORTCUT_APP_LIST = 1;
	public static final int SHORTCUT_TITLE = 2;
	public static final int SHORTCUT_OPEN = 3;

	// Shortcut Action
	public static final int ACTION_JUMP_BACKWARD = 0;
	public static final int ACTION_JUMP_FORWARD = 1;
	public static final int ACTION_PREV = 2;
	public static final int ACTION_NEXT = 3;
	public static final int ACTION_PLAY_AND_PAUSE = 4;
	public static final int ACTION_PAUSE = 5;
	public static final int ACTION_STOP = 6;
	public static final int ACTION_FULL_SCREEN = 7;
	public static final int ACTION_DOWN = 8;
	public static final int ACTION_UP = 9;
	public static final int ACTION_MUTE = 10;
	public static final int ACTION_SUBTITLE = 11;
	public static final int ACTION_LEFT = 12;
	public static final int ACTION_RIGHT = 13;
	// trump
	public static final int ACTION_CLOVER = 14;
	public static final int ACTION_HEART = 15;
	public static final int ACTION_DIAMOND = 16;
	public static final int ACTION_SPADE = 17;
	//subtitle
	public static final int ACTION_SUBTITLE_BACKWARD = 18;
	public static final int ACTION_SUBTITLE_RESET = 19;
	public static final int ACTION_SUBTITLE_FORWARD = 20;
	//audio
	public static final int ACTION_AUDIO_BACKWARD = 21;
	public static final int ACTION_AUDIO_RESET = 22;
	public static final int ACTION_AUDIO_CHANGE = 23;
	public static final int ACTION_AUDIO_FORWARD = 24;
	// playstation
	public static final int ACTION_CIRCLE= 25;
	public static final int ACTION_CROSS = 26;
	public static final int ACTION_RECTANGLE = 27;
	public static final int ACTION_TRIANGLE = 28;

	public static final int ACTION_POWER = 0x0100;
	public static final int ACTION_FOCUS = 0x0200;
	public static final int ACTION_SHUTDOWN = 0x0300;
	public static final int ACTION_RESTART = 0x0400;
	public static final int ACTION_PING = 0x0500;
	public static final int ACTION_VOLUME_UP = 0x0600;
	public static final int ACTION_VOLUME_DOWN = 0x0700;
	public static final int ACTION_HIDE = 0x0800;
	public static final int ACTION_SHOW_HIDE = 0x1000;
	
	// Mouse Action
	public static final int ACTION_MOVE = 0x00000001;
	public static final int ACTION_LBUTTONDOWN = 0x00000002;
	public static final int ACTION_MBUTTONDOWN = 0x00000004;
	public static final int ACTION_RBUTTONDOWN = 0x00000008;
	public static final int ACTION_LBUTTONUP = 0x00000020;
	public static final int ACTION_MBUTTONUP = 0x00000040;
	public static final int ACTION_RBUTTONUP = 0x00000080;
	public static final int ACTION_LBUTTONCLICK = 0x00000022;
	public static final int ACTION_MBUTTONCLICK = 0x00000044;
	public static final int ACTION_RBUTTONCLICK = 0x00000088;
	public static final int ACTION_VSCROLL = 0x00001000;
	public static final int ACTION_HSCROLL = 0x00002000;
	public static final int ACTION_MAGNIFIER_START = 0x01000000;
	public static final int ACTION_MAGNIFIER_STOP = 0x02000000;
	
	// System SubType
	public static final int SYSTEM_SHUTDOWN = 0;
	public static final int SYSTEM_REBOOT = 1;
	public static final int SYSTEM_LOGOFF = 2;
	public static final int SYSTEM_HIBERNATE = 3;
	public static final int SYSTEM_LOCK_SCREEN = 4;
	public static final int SYSTEM_GET_VOLUME = 5;
	public static final int SYSTEM_MONITOR = 6;
	public static final int SYSTEM_SET_VOLUME = 7;
	public static final int SYSTEM_SUSPEND = 8;
	public static final int SYSTEM_NON_SLEEPING_MODE = 9;
	
	// FileBrowse SubType
	public static final int FILE_BROWSE_CD = 0;
	public static final int FILE_BROWSE_DELETE = 1;
	public static final int FILE_BROWSE_LIST = 2;
	public static final int FILE_BROWSE_CWD = 3;
	public static final int FILE_BROWSE_RENAME = 4;

	private static final int NONE = 0;
	private static final int INIT = 1;
	private static final int READY = 2;

	public static final int DEFAULT_SERVER_PORT = 1028;
	private static final int MIN_COMMAND_MESSAGE_SIZE = 16;
	private static final int BUFFER_SIZE = 65536;
	private static RemoteControl self = null;

	// For connecting to a PC server.
	private int state;
	private DatagramSocket socket = null;
	private DatagramPacket recvPacket = null;

	private InetAddress serverAddress = null;
	private int serverPort;

	private WifiManager wifiManager;

	private int session;

	private int[] keyCodes = new int[4];
	private int timeout;
	private int portTryCount;
	
	private byte[] mac;

	private Context context;
	private SharedPreferences prefs;
	
	private MessageCreator messageCreator;
	
	public RemoteControl() {
		messageCreator = new MessageCreator(BUFFER_SIZE);
		ByteBuffer byteBuffer = messageCreator.getByteBuffer();
		recvPacket = new DatagramPacket(byteBuffer.array(), byteBuffer.capacity());

		portTryCount = 3;
		session = 0;
		state = NONE;
	}

	public static RemoteControl getInstance() {
		if(self == null) {
			self = new RemoteControl();
		}
		
		return self;
	}
	
	private void setSession(final int session) {
		this.session = session;
		messageCreator.setSession(session);
	}
	
	/**
	 * If Wifi is not active, it returns null.
	 * @return
	 */
	public String getSSID() {
		String ssid = null;
		
		if(wifiManager != null) {
			WifiInfo wifiInfo = wifiManager.getConnectionInfo();

			if(wifiInfo != null) {
				ssid = wifiInfo.getSSID();
			}
		}
		
		return ssid;
	}
	
	public byte[] getServerMacAddress() {
		return mac;
	}

	public boolean initialize(Context context) {
		if(state != NONE) {
			return false;
		}
		
		this.context = context.getApplicationContext();
		
		wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		
		prefs = PreferenceManager.getDefaultSharedPreferences(context);
		timeout = Utils.getIntGreaterThanZeroFromPrefs(
			prefs, "connection_timeout", Constant.CONNECTION_TIMEOUT_DEFAULT);

		// get a stored server port from SharedPreferences SETTINGS.
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		String ssid = wifiInfo.getSSID();
		if (ssid == null) {
			ssid = "";
		}
		serverPort = prefs.getInt(ssid + "_port", DEFAULT_SERVER_PORT);
		if(serverPort < 1 || serverPort > 0xFFFF) {
			serverPort = DEFAULT_SERVER_PORT;
		}
		 
		 createSocket();

		 state = INIT;

		return true;
	}
	
	public boolean createSocket() {
		try {
			if(socket != null) {
				socket.close();
			}
			
			socket = new DatagramSocket();
		} catch (SocketException e) {
			e.printStackTrace();
			Log.e(TAG, e.toString());
			return false;
		}	
		
		return true;
	}

	public boolean deinitialize() {
		if(state == NONE) {
			return false;
		}

		if (socket != null) {
			socket.close();
			socket = null;
		}

		state = NONE;

		return true;
	}
	
	/**
	 * This method is only called in the case that wifi turns on.
	 * @param serverList
	 * @return
	 */
	public int detectServer(List<ServerInfo> serverList) {
		serverList.clear();
		
		ByteBuffer byteBuffer = ByteBuffer.allocate(BUFFER_SIZE);
		byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
		final DatagramPacket recvPacket = new DatagramPacket(byteBuffer.array(), BUFFER_SIZE);

		try {	
			final DatagramPacket sendPacket = createServerDetectionPacket();

			int size;
			int port = DEFAULT_SERVER_PORT;
			socket.setBroadcast(true);
			
			for (int i = 0; i < portTryCount; i++, port++) {
				sendPacket.setPort(port);
				socket.send(sendPacket);

				try {
					size = receivePacket(recvPacket, timeout, 20);
				} catch(IOException ioe) {
					continue;
				}
				
				byteBuffer.position(0);
				final int type = byteBuffer.getInt();
				final int length = byteBuffer.getInt();
				final int session = byteBuffer.getInt(); // This value has no meaning at this moment.
				final int subType = byteBuffer.getInt();
				
				if(type != CMD_SERVER || subType != SERVER_DETECTION || size != length) {
					continue;
				}

				final String name = getStringFromBuffer(byteBuffer);
				final InetAddress addr = recvPacket.getAddress();

				final ServerInfo serverInfo = createServerInfo(name, addr, port);
				serverList.add(serverInfo);
			}

		} catch (UnknownHostException uhe) {
			uhe.printStackTrace();
			Log.e("ERROR", "Failed to create an address.");
			return Result.ERROR_UNKNOWN_HOST;
		} catch (SocketException e) {
			e.printStackTrace();
			return Result.ERROR_SOCKET;
		} catch(Exception e) {
			e.printStackTrace();
			return Result.ERROR_UNKNOWN;
		}

		return serverList.size();
	}
	
	private ServerInfo createServerInfo(final String name, final InetAddress address, final int port) {
		ServerInfo serverInfo = new ServerInfo();
		serverInfo.setName(name);
		serverInfo.setIp(address.getAddress());
		serverInfo.setPort(port);
		
		return serverInfo;
	}
	
	private InetAddress getBroadcastAddress() throws Exception {
		DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();
		
		int ip = dhcpInfo.ipAddress;
		int netmask = dhcpInfo.netmask;
		byte[] broadcastIP = new byte[4];
		Utils.getBroadcastIP(ip, netmask, broadcastIP);

		InetAddress address = InetAddress.getByAddress(broadcastIP);
		return address;
	}
	
	private DatagramPacket createServerDetectionPacket() throws Exception {
		InetAddress address = getBroadcastAddress();
		
		final ByteBuffer messageBuffer = messageCreator.getServerDetectionData();
		final int size = messageBuffer.position();
		
		DatagramPacket packet = new DatagramPacket(messageBuffer.array(), size);
		packet.setAddress(address);
		
		return packet;
	}
	
	private String getStringFromBuffer(ByteBuffer byteBuffer) {
		final int textLength = byteBuffer.getInt();
		final int start = byteBuffer.position();
		final byte[] buffer = byteBuffer.array();
		
		String text = "";
		try {
			text = new String(buffer, start, textLength, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		byteBuffer.position(start + textLength);
		
		return text;
	}

	public int authenticate(String hostName, final int port, final String password) {
		if(hostName == null) {
			return Result.ERROR_UNKNOWN_HOST;
		}
		
		byte[] ip = Utils.getIpByName(hostName);
		if(ip == null) {
			return Result.ERROR_UNKNOWN_HOST;
		}

		if(port < 1028 || port > 0xFFFF) {
			return Result.ERROR_INVALID_PORT;
		}
		
		return authenticate(ip, port, password);
	}

	/**
	 * Request=Type(4):Length(4):Session(4):SubType(4):MD5(16)
	 * Response=Type(4):Length(4):Session(4):SubType(4):Result(4):Mac(6)
	 * 
	 * @param ip
	 * @return
	 */
	public int authenticate(byte[] ip, int port, final String password) {
		state = INIT;

		try {
			port = Utils.checkPort(port);
			InetAddress address = InetAddress.getByAddress(ip);
			ByteBuffer messageBuffer = messageCreator.getServerAuthenticateData(password);

			DatagramPacket packet = createDatagramPacket(messageBuffer, address, port);
			sendPacket(packet, false);

			final int size = receivePacket(recvPacket, timeout, 20);

			messageBuffer.position(0);
			final int type = messageBuffer.getInt();
			final int length = messageBuffer.getInt();
			final int session = messageBuffer.getInt();
			final int subType = messageBuffer.getInt();
			final int success = messageBuffer.getInt();
			
			if(type != CMD_SERVER || subType != SERVER_AUTHENTICATE || size != length) {
				return Result.ERROR_UNKNOWN;
			}
			
			if(success == 0) {
				return Result.ERROR_AUTHENTICATION_FAILURE;
			}

			// Mac address exists.
			if(size >= 26) {
				mac = new byte[6];
				messageBuffer.get(mac);
			}
			else {
				mac = null;
			}

			setSession(session);
			serverAddress = packet.getAddress();
			serverPort = port;

			state = READY;

			return Result.OK;

		} catch (UnknownHostException e1) {
			e1.printStackTrace();
			return Result.ERROR_UNKNOWN_HOST;
		} catch (SocketException e) {
			e.printStackTrace();
			return Result.ERROR_SOCKET;
		} catch (IOException e) {
			e.printStackTrace();
			return Result.ERROR_UNKNOWN;
		}
	}

	private int sendPacket(final ByteBuffer byteBuffer, boolean broadcast) {
		if (state != READY) {
			return -1;
		}

		int size = byteBuffer.position();
		DatagramPacket packet = new DatagramPacket(byteBuffer.array(), size,
				serverAddress, serverPort);

		try {
			sendPacket(packet, broadcast);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return 0;
	}
	
	private int sendPacket(final ByteBuffer byteBuffer) {
		return sendPacket(byteBuffer, false);
	}
	
	private void sendPacket(final DatagramPacket packet, final boolean broadcast) throws IOException {
		socket.setBroadcast(broadcast);
		socket.send(packet);
	}
	
	private int receivePacket(final DatagramPacket packet, final int timeout) throws IOException {
		packet.setLength(BUFFER_SIZE);

		socket.setSoTimeout(timeout);
		socket.receive(packet);
		
		return packet.getLength();
	}
	
	private int receivePacket(
			final DatagramPacket packet, final int timeout, final int minRecvSize) throws IOException {
		final int size = receivePacket(packet, timeout);
		
		if(size < minRecvSize) {
			throw new IOException("Data is wrong. The size is too small.");
		}
		
		return size;
	}
	
	private DatagramPacket createDatagramPacket(final ByteBuffer byteBuffer) {
		return createDatagramPacket(byteBuffer, serverAddress, serverPort);
	}
	
	private DatagramPacket createDatagramPacket(
			final ByteBuffer byteBuffer, final InetAddress address, final int port) {
		final int size = byteBuffer.position();
		DatagramPacket packet = new DatagramPacket(
				byteBuffer.array(), size, address, port);
		
		return packet;		
	}
	
	public void setApp(int appID) {
		if (state != READY) {
			return;
		}

		ByteBuffer sendBuffer = messageCreator.getSetAppData(appID);
		sendPacket(sendBuffer);
	}

	public void controlShortcut(int appID, int action) {
		if (state != READY) {
			return;
		}

		final int keyStatus = 0;
		controlShortcut(appID, keyStatus, action);
	}

	/**
	 * ShorcutRequest=Type(4):Length(4):Session(4):SubType(4):AppID(4):KeyStatus
	 * (4):Shortcut(4)
	 * 
	 * @param appID
	 * @param action
	 */
	public void controlShortcut(int appID, int keyStatus, int action) {
		if (state != READY) {
			return;
		}

		ByteBuffer sendBuffer = messageCreator.getControlShortcutData(appID, keyStatus, action);
		sendPacket(sendBuffer);
	}

	/**
	 * Request=Type(4):Length(4):Session(4):SubType(4):undefined
	 * 
	 * @param action
	 * @param x
	 * @param y
	 */
	public void controlSystem(int subType) {
		if (state != READY) {
			return;
		}

		ByteBuffer sendBuffer = messageCreator.getControlSystemData(subType);
		sendPacket(sendBuffer);
	}
	
	public boolean wakeOnLan() {
		final String ssid = getSSID();
		
		final long mac = prefs.getLong(ssid+"_mac", 0);
		if(mac == 0) {
			return false;
		}

		return OnLanWaker.run(mac);
	}

	/**
	 * Request=Type(4):Length(4):Session(4):Action(4):x(4):y(4)
	 * 
	 * @param action
	 * @param x
	 * @param y
	 */
	public void controlMouse(int action, int x, int y) {
		if (state != READY) {
			return;
		}

		ByteBuffer sendBuffer = messageCreator.getControlMouseData(action, x, y);
		sendPacket(sendBuffer);
	}
	
	public void startMagnifier() {
		controlMouse(ACTION_MAGNIFIER_START, 0, 0);
	}
	
	public void stopMagnifier() {
		controlMouse(ACTION_MAGNIFIER_STOP, 0, 0);
	}

	/**
	 * Request=Type(4):Length(4):Session(4):KeyCount(4):KeyCodes(4):KeyCodes(4):
	 * ...
	 */
	public void controlKeyboard(final int count, final int[] keyCodes) {
		if (state != READY) {
			return;
		}

		ByteBuffer sendBuffer = messageCreator.getControlKeyboardData(count, keyCodes);
		sendPacket(sendBuffer);
	}

	public void controlKeyboard(int keyCode) {
		if (state != READY) {
			return;
		}

		final int count = 1;
		this.keyCodes[0] = keyCode;

		controlKeyboard(count, keyCodes);
	}

	/**
	 * AppListRequest=Type(4):Length(4):Session(4):SubType(4)
	 * AppListResponse=Type
	 * (4):Length(4):Session(4):SubType(4):AppCount(4):TextSize
	 * (4):Text:TextSize(4):Text:TextSize(4):Text:...
	 * 
	 * @param appNames
	 * @return
	 */
	public synchronized int  getAppList(List<String> appNames, int[] focusedAppID) {
		if (state != READY) {
			return -1;
		}

		appNames.clear();

		ByteBuffer messageBuffer = messageCreator.getGetAppListData();
		DatagramPacket packet = createDatagramPacket(messageBuffer);

		try {
			sendPacket(packet, false);
			int size = receivePacket(recvPacket, timeout);

			if (size < 20) {
				// Wrong response
				return -1;
			}

			// APP Count
			messageBuffer.position(0);			
			final boolean isValid =
					isMessageHeaderValid(messageBuffer, CMD_SHORTCUT, SHORTCUT_APP_LIST, size);
			
			if( !isValid ) {
				return -2;
			}

			// AppCount
			final int appCount = messageBuffer.getInt();
			String appName;

			for(int i = 0; i < appCount; i++) {
				// AppName
				appName = getStringFromBuffer(messageBuffer);
				appNames.add(appName);
			}
			
			int appID = -1;
			int position = messageBuffer.position();
			
			if(size >= position + 4) {
				appID = messageBuffer.getInt();
			}
			
			focusedAppID[0] = appID;

		} catch (IOException e) {
			e.printStackTrace();
			Log.e(TAG, e.toString());
		}

		return appNames.size();
	}
	
	/**
	 * Get the title of the current focused application. 
	 * @return
	 */
	public String getTitle(int appID) {
		if (state != READY) {
			return null;
		}
		
		String title = null;

		ByteBuffer messageBuffer = messageCreator.getGetTitleData(appID);
		DatagramPacket packet = createDatagramPacket(messageBuffer);

		try {
			socket.send(packet);
			final int size = receivePacket(recvPacket, timeout, 20);

			final boolean isHeaderValid = isMessageHeaderValid(
					messageBuffer, CMD_SHORTCUT, SHORTCUT_TITLE, size);
			if( !isHeaderValid ) {
				return null;
			}
			
			int result = messageBuffer.getInt();
			if(result < 0) {
				title = getTitleErrorMessage(result);
			}
			else {
				// Application's Title
				title = getStringFromBuffer(messageBuffer);
			}

		} catch(Exception e) {
			Log.e(TAG, e.toString());
			title = context.getResources().getString(R.string.no_response_error);
		}

		return title;
	}
	
	private String getTitleErrorMessage(final int result) {
		final Resources res = context.getResources();
		String text;
		
		switch(result) {
		case -1:
			text = res.getString(R.string.no_focus_error);
			break;
		case -2:
			text = res.getString(R.string.no_title_error);
			break;
		default:
			text = res.getString(R.string.unexpected_error);
			break;
		}
		
		return text;
	}
	
	/**
	 * Send a text in UTF-8 to the server.
	 * The server will convert the text to unicode format. 
	 * @param text
	 */
	public void sendText(final String text) {
		if(state != READY) {
			return;
		}
		
		ByteBuffer sendBuffer = messageCreator.getSendTextData(text);

		if(sendBuffer != null) {
			sendPacket(sendBuffer);
		}
	}
	
	/**
	 * Request=Type(4):Length(4):Session(4):SubType(4)
	 * Response=Type(4):Length(4):Session(4):SubType(4):Result(4):Volume(4):Min(4):Max(4)
	 * 
	 * @param volume
	 * @return
	 */
	public boolean getVolume(final int[] volume) {
		if(state != READY) {
			return false;
		}
		
		ByteBuffer messageBuffer = messageCreator.getGetVolumeData(volume);
		DatagramPacket packet = createDatagramPacket(messageBuffer);

		try {
			socket.send(packet);
			final int size = receivePacket(recvPacket, timeout, 20);

			final boolean isValid = isMessageHeaderValid(
					messageBuffer, CMD_SYSTEM_CONTROL, SYSTEM_GET_VOLUME, size);
			if( !isValid ) {
				return false;
			}
			
			int result = messageBuffer.getInt();
			if(result < 0) {
				return false;
			}
			
			volume[0] = messageBuffer.getInt(); // Volume
			volume[1] = messageBuffer.getInt(); // Minimum
			volume[2] = messageBuffer.getInt(); // Maximum
			
			return true;

		} catch (IOException e) {
			e.printStackTrace();
			Log.e(TAG, e.toString());
		} catch(Exception e) {
			Log.e(TAG, e.toString());
		}

		return false;
	}
	
	/**
	 * Request=Type(4):Length(4):Session(4):SubType(4):Volume(4)
	 * 
	 * @param volume
	 */
	public void setVolume(final int volume) {
		if(state != READY) {
			return;
		}
		
		ByteBuffer sendBuffer = messageCreator.getSetVolumeData(volume);
		sendPacket(sendBuffer);
	}
		
	/**
	 * Request=Type(4):Length(4):Session(4):SubType(4):AppID(4):TextLength(4):ByteSize(4):Text(UTF-8)
	 * Response=Type(4):Length(4):Session(4):SubType(4)
	 * 
	 * @param volume
	 */
	public boolean open(int appID, String pathname) {
		if (state != READY) {
			return false;
		}

		ByteBuffer sendBuffer = messageCreator.getOpenData(appID, pathname);
		if(sendBuffer != null) {
			sendPacket(sendBuffer);
			return true;
		}
		
		return false;
	}

	/**
	 * Request=Type(4):Length(4):Session(4):SubType(4)
	 * Response=Type(4):Length(4):Session(4):SubType(4):TextSize(4):Text(UTF-8)
	 * 
	 * @param volume
	 */
	public String cwd() {
		if (state != READY) {
			return null;
		}
		
		ByteBuffer messageBuffer = messageCreator.getCwdData();
		DatagramPacket packet = createDatagramPacket(messageBuffer);
		
		try {
			socket.send(packet);
			final int size = receivePacket(recvPacket, timeout, 20);
			
			final boolean isValid = isMessageHeaderValid(
					messageBuffer, CMD_FILE_BROWSE, FILE_BROWSE_CWD, size);
			if( !isValid ) {
				return null;
			}
			
			final String cwd = getStringFromBuffer(messageBuffer);
			return cwd;

		} catch(Exception e) {
			Log.e(TAG, e.toString());
		}

		return null;
	}
	
	/**
	 * Request=Type(4):Length(4):Session(4):SubType(4):TextSize(4):Text(UTF-8)
	 * Response=Type(4):Length(4):Session(4):SubType(4):Flag(2):Count(2):FileType(1):TextSize(4):Text:FileType(4):TextSize(4):Text:TextSize(4):Text:...
	 * @param pathname
	 * @param fileList
	 * @return
	 */
	public int list(String pathname, LinkedList<FileInfo> fileList) {
		if (state != READY) {
			return -1;
		}
		
		fileList.clear();
		
		final int timeout = 3000;
		final ByteBuffer messageBuffer = messageCreator.getListData(pathname);
		final DatagramPacket packet = createDatagramPacket(messageBuffer);
		
		try {
			socket.send(packet);
			
			int oldSequence = -1;

			while(true) {
				final int size = receivePacket(recvPacket, timeout, 20);
				
				final boolean isValid = isMessageHeaderValid(
						messageBuffer, CMD_FILE_BROWSE, FILE_BROWSE_LIST, size);
				if( !isValid ) {
					return -2;
				}
				
				// Flag(1)
				short flag = messageBuffer.getShort();
				boolean error = ((flag & 0x400) != 0);
				if(error) {
					return -3;
				}
				
				boolean end = ((flag & 0x100) != 0);
				boolean start = ((flag & 0x200) != 0); 
				int sequence = flag & 0xFF;
				
				if(oldSequence > -1) {
					if(sequence != (oldSequence + 1) % 0x10) {
						printSequenceErrorMessage(oldSequence, sequence);
					}
				}
				else {
					if(oldSequence < 0 && !start) {
						Log.e("JAVA", "The first packet was dropped.");
					}
				}
				oldSequence = sequence;
				
				// # of files
				final int count = messageBuffer.getShort();
				
				for(int i=0; i<count; i++) {
					FileInfo fileInfo = createFileInfo(messageBuffer);
					fileList.addLast(fileInfo);				
				}
				
				if(end) {
					break;
				}
			}

			return fileList.size();

		} catch (IOException e) {
			e.printStackTrace();
		} catch(Exception e) {
			Log.e(TAG, e.toString());
		}
		
		return -4;
	}
	
	private void printSequenceErrorMessage(final int oldSequence, final int sequence) {
		StringBuilder sb = new StringBuilder();
		sb.append("Seq is wrong: ");
		sb.append("old(");
		sb.append(oldSequence);
		sb.append(") new(");
		sb.append(sequence);
		sb.append(")");
		
		Log.e("JAVA", sb.toString());
	}
	
	private FileInfo createFileInfo(ByteBuffer messageBuffer) {
		String name;
		final int type = (int) messageBuffer.get();
		final String longName = getStringFromBuffer(messageBuffer);
		
		if(type == FileInfo.DRIVE) {
			int index = longName.indexOf(":");
			name = longName.substring(0, index+1); 
		}
		else {
			name = longName;
		}
		
		return new FileInfo(type, name, longName);
	}
	
	public boolean setNonSleepingMode(final int on) {
		if(state != READY) {
			return false;
		}

		final ByteBuffer messageBuffer = messageCreator.getSetNonSleepingMode(on);
		final DatagramPacket packet = createDatagramPacket(messageBuffer);
		
		try {
			socket.send(packet);
			final int size = receivePacket(recvPacket, timeout, 20);
			
			final boolean isValid = isMessageHeaderValid(
					messageBuffer, CMD_SYSTEM_CONTROL, SYSTEM_NON_SLEEPING_MODE, size); 
			if( !isValid ) {
				return false;
			}

			final int responsedNonSleepingMode = messageBuffer.getInt();		
			return (on == responsedNonSleepingMode);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	private boolean isMessageHeaderValid(
			final ByteBuffer byteBuffer,
			final int commandType, final int commandSubType, final int packetSize) {
		
		if(packetSize < MIN_COMMAND_MESSAGE_SIZE) {
			return false;
		}
	
		byteBuffer.position(0);
		
		int type = byteBuffer.getInt();
		if(type != commandType) {
			return false;
		}
		
		int length = byteBuffer.getInt();
		if(length != packetSize) {
			return false;
		}
		
		int session = byteBuffer.getInt();
		if(session != this.session) {
			return false;
		}
		
		int subType = byteBuffer.getInt();
		if(subType != commandSubType) {
			return false;
		}
		
		return true;
	}
}