package com.goldworm.net;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

public class RemoteControlThread implements Runnable, Handler.Callback {
	
	private final int INIT = 0;
	private final int RUN = 1;
	
	public static final int QUIT = -1; 
	public static final int CONTROL_SHORTCUT = 0;
	public static final int TITLE = 1;
	public static final int CWD = 2;
	public static final int GET_VOLUME = 3;
	public static final int SET_VOLUME = 4;
	public static final int WAKE_ON_LAN = 5;
	public static final int NON_SLEEPING_MODE = 6;
	public static final int CONTROL_KEYBOARD = 7;
	public static final int TEXT = 8;
	public static final int CONTROL_MOUSE = 9;
	public static final int MAGNIFIER = 10;
	
	private int state = INIT;
	private Thread thread;
	private Handler handler;
	private RemoteControl remoteControl;
	private static RemoteControlThread self = null;
	
	private class Callback {
		private Handler target;
		
		public Callback(final Handler target) {
			this.target = target;
		}
		
//		public Callback(final Handler target, Object obj) {
//			this.target = target;
//			this.obj = obj;
//		}
	}
	
	public static RemoteControlThread getInstance() {
		if(self == null) {
			self = new RemoteControlThread();
		}
		
		return self;
	}

	public RemoteControl getControl() {
		return remoteControl;
	}
	
	public synchronized void start() {
		if(state == INIT) {
			thread = new Thread(this);
			thread.start();
			
			state = RUN;
		}
	}
	
	public synchronized void quit() {
		if(state == INIT) {
			return;
		}
		
		handler.sendEmptyMessage(QUIT);
		
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		thread = null;
		state = INIT;
	}
	
	public void controlKeyboard(int keyCode) {
		int[] keyCodes = new int [1];
		keyCodes[0] = keyCode;
		
		controlKeyboard(1, keyCodes);
	}
	
	public void controlKeyboard(final int count, final int[] keyCodes) {
		Message msg = handler.obtainMessage(CONTROL_KEYBOARD, keyCodes);
		msg.arg1 = count;

		handler.sendMessage(msg);
	}
	
	public void startMagnifier() {
		controlMouse(RemoteControl.ACTION_MAGNIFIER_START, 0, 0);
	}
	
	public void stopMagnifier() {
		controlMouse(RemoteControl.ACTION_MAGNIFIER_STOP, 0, 0);
	}
	
	public void controlMouse(final int action, final int x, final int y) {
		int[] values = new int [3];
		values[0] = action;
		values[1] = x;
		values[2] = y;
		
		Message msg = handler.obtainMessage(CONTROL_MOUSE, values);
		handler.sendMessage(msg);
	}
	
	public void sendText(final String text) {
		Message msg = handler.obtainMessage(TEXT, text);
		handler.sendMessage(msg);		
	}

	public void controlShortcut(int appID, int action) {
		final int keyStatus = 0;
		controlShortcut(appID, keyStatus, action);
	}

	public void controlShortcut(final int appID, final int keyStatus, final int action) {
		ArrayList<Integer> values = new ArrayList<Integer> (3);
		values.add(appID);
		values.add(keyStatus);
		values.add(action);
		
		Message msg = handler.obtainMessage(CONTROL_SHORTCUT, values);		
		
		handler.sendMessage(msg);
	}
	
	public void getTitle(final Handler target, final int appID) {
		Callback callback = new Callback(target);
		
		Message msg = handler.obtainMessage(TITLE, appID, 0);
		msg.obj = callback;
	
		handler.sendMessage(msg);
	}
	
	public void cwd(final Handler target) {
		Callback callback = new Callback(target);
		
		Message msg = handler.obtainMessage(CWD);
		msg.obj = callback;
		
		handler.sendMessage(msg);
	}
	
	public void getVolume(final Handler target) {
		Callback callback = new Callback(target);
		
		Message msg = handler.obtainMessage(GET_VOLUME);
		msg.obj = callback;
		
		handler.sendMessage(msg);		
	}
	
	public void setVolume(final int volume) {
		Message msg = handler.obtainMessage(SET_VOLUME, volume, 0);
		handler.sendMessage(msg);
	}
	
	public void wakeOnLan() {
		handler.sendEmptyMessage(WAKE_ON_LAN);
	}
	
	public void setNonSleepingMode(final Handler target, final int on) {
		Callback callback = new Callback(target);
		Message msg = handler.obtainMessage(NON_SLEEPING_MODE, on, 0, callback);
		handler.sendMessage(msg);		
	}

	@Override
	public void run() {
		Log.d("async", "RemoteControlThread START");
		
		Looper.prepare();
		
		remoteControl = RemoteControl.getInstance();
		handler = new Handler(this);
		
		Looper.loop();
		
		Log.d("async", "RemoteControlThread END");
	}

	@Override
	public boolean handleMessage(Message msg) {
		final int command = msg.what; 
		
		switch(command) {
		case QUIT:
			final Looper looper = Looper.myLooper();
			if(looper != null) {
				looper.quit();
			}
			return true;
			
		case CONTROL_SHORTCUT:
			return handleShortcut(msg);
		
		case TITLE:
			return handleTitle(msg);
			
		case CWD:
			return handleCwd(msg);
			
		case GET_VOLUME:
			return handleGetVolume(msg);
			
		case SET_VOLUME:
			return handleSetVolume(msg);
			
		case WAKE_ON_LAN:
			return handleWakeOnLan(msg);
			
		case NON_SLEEPING_MODE:
			return handleNonSleepingMode(msg);
			
		case CONTROL_KEYBOARD:
			return handleKeyboard(msg);
			
		case CONTROL_MOUSE:
			return handleMouse(msg);
			
		case TEXT:
			return handleText(msg);
		}
		
		return false;
	}

	private boolean handleText(Message msg) {
		final String text = (String) msg.obj;
		
		if(text != null) {
			remoteControl.sendText(text);
			return true;
		}
		
		return false;
	}

	private boolean handleMouse(Message msg) {
		final int[] values = (int[]) msg.obj;
		
		if(values != null) {
			// action, x, y
			remoteControl.controlMouse(values[0], values[1], values[2]);
			return true;
		}
		
		return false;
	}

	private boolean handleKeyboard(Message msg) {
		final int count = msg.arg1;
		int[] keyCodes = (int[]) msg.obj;
		
		if(keyCodes != null) {
			remoteControl.controlKeyboard(count, keyCodes);
			return true;
		}
		
		return false;
	}

	private boolean handleNonSleepingMode(Message msg) {
		final int on = msg.arg1;
		final boolean success = remoteControl.setNonSleepingMode(on);
		
		Callback callback = (Callback) msg.obj;
		Handler target = callback.target;
		Message relayMsg = target.obtainMessage(msg.what);
		relayMsg.arg1 = on;
		relayMsg.arg2 = success ? 1 : 0;
		
		target.sendMessage(relayMsg);
		
		return true;
	}

	private boolean handleWakeOnLan(Message msg) {
		remoteControl.wakeOnLan();
		return true;
	}
	
	private boolean handleGetVolume(Message msg) {
		int[] volume = new int [3];
		final boolean success = remoteControl.getVolume(volume);
		
		Callback callback = (Callback) msg.obj;
		Handler target = callback.target;
		Message replyMsg = target.obtainMessage(msg.what, volume); 
		replyMsg.arg1 = success ? 1 : 0;
		
		target.sendMessage(replyMsg);
		
		return true;
	}

	private boolean handleSetVolume(Message msg) {
		final int volume = msg.arg1;
		remoteControl.setVolume(volume);
		
		return true;
	}

	private boolean handleCwd(Message msg) {
		final String cwd = remoteControl.cwd();
		
		Callback callback = (Callback) msg.obj;
		Handler target = callback.target;
		Message replyMsg = target.obtainMessage(msg.what, cwd);
		replyMsg.arg1 = (cwd == null) ? 0 : 1;	// cwd()'s success or failure
		target.sendMessage(replyMsg);
		
		return true;
	}

	private boolean handleTitle(Message msg) {
		final int appID = msg.arg1;
		final String title = remoteControl.getTitle(appID);
		
		Callback callback = (Callback) msg.obj;
		
		Handler target = callback.target;
		Message replyMsg = target.obtainMessage(msg.what, title);
		replyMsg.arg1 = (title == null) ? 0 : 1;	// getTitle()'s success or failure
		target.sendMessage(replyMsg);		
		
		return true;
	}

	private boolean handleShortcut(Message msg) {
		
		if(msg.obj instanceof ArrayList)
		{
			@SuppressWarnings("unchecked")
			ArrayList<Integer> values = (ArrayList<Integer>) msg.obj;
			final int appID = values.get(0);
			final int keyStatus = values.get(1);
			final int action = values.get(2);
			
			remoteControl.controlShortcut(appID, keyStatus, action);
			
			return true;
		}
		
		return false;
	}
}
