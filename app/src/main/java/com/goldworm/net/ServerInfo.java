package com.goldworm.net;

public class ServerInfo {
	private byte[] ip;
	private int port;
	private String name;
	
	public ServerInfo() {
		ip = new byte[4];
	}

	public byte[] getIp() {
		return ip;
	}
	public void setIp(byte[] ip) {
		this.ip = ip;
//		System.arraycopy(ip, 0, this.ip, 0, ip.length);
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	

}
