package com.goldworm.remotecontrol;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import com.goldworm.net.RemoteControl;
import com.goldworm.net.ServerInfo;
import com.goldworm.remotecontrol.ServerSearcher.OnSearchListener;
import com.goldworm.utils.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ConnectActivity extends Activity implements OnClickListener, OnSearchListener {
	
	private EditText ipEdit;
	private EditText portEdit;
	private EditText passEdit;
	private TextView ssidTextView;
	
	private WifiManager wifiManager;
	private WifiMonitor wifiMonitor;
	private SharedPreferences prefs;
	
	private Dialog loadingDialog;
	private ServerOnClickListener serverOnClickListener = new ServerOnClickListener();
	private ServerSearcher serverSearcher;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(R.string.connect_title);
		setContentView(R.layout.connect);
		
		ipEdit = (EditText) this.findViewById(R.id.IPEditText);
		portEdit = (EditText) this.findViewById(R.id.PortEditText);
		passEdit = (EditText) this.findViewById(R.id.PassEditText);
		ssidTextView = (TextView) this.findViewById(R.id.ssid_textview);

		final int[] buttonIds = {
			R.id.connect_button,
			R.id.server_detection_button,
			R.id.wol_button,
			R.id.ssid_button,
		};
		
		for(int i=0; i<buttonIds.length; i++) {
			Button button = (Button) this.findViewById(buttonIds[i]);
			
			if(button != null) {
				button.setOnClickListener(this);
			}
		}
		
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		
		initWifiMonitor();
		
		wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
		
		Intent intent = this.getIntent();
		String address = intent.getStringExtra("address");
		if(address != null && address.length() > 0) {
			int port = intent.getIntExtra("port", RemoteControl.DEFAULT_SERVER_PORT);
			String password = intent.getStringExtra("password");
			
			setConfiguration(address, port, password);
		}
		else {
			setDefaultConfiguration();
		}
		
		initServerDetectionUI();
	}
	
	private void initWifiMonitor() {
		wifiMonitor = new WifiMonitor(this);
		
		IntentFilter filter = new IntentFilter();
		filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
		filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
		this.registerReceiver(wifiMonitor, filter);
	}
	
	private void initServerDetectionUI() {
		serverSearcher = new ServerSearcher();
		serverSearcher.setOnSearchListener(this);
		
		loadingDialog = createLoadingDialog();
	}
	
	public Dialog createLoadingDialog() {
		Resources res = this.getResources();
		ProgressDialog loadingDialog = new ProgressDialog(this);
        loadingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        loadingDialog.setMessage(res.getString(R.string.detecting_server));
        loadingDialog.setCancelable(false);
	    
        return loadingDialog;		
	}
	
	public Dialog createServerSelectionDialog(List<ServerInfo> servers) {
		int i;
		final int size = servers.size();
		if(size == 0) {
			return null;
		}
		
		AlertDialog.Builder builder;
		
		ServerInfo serverInfo;
		String[] serverNames = new String [size];
		for(i=0; i<size; i++)
		{
			serverInfo = servers.get(i);
			serverNames[i] = serverInfo.getName();
		}
		
		serverOnClickListener.setServers(servers);

		builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.server_selection);
	    builder.setCancelable(true);
	    builder.setItems(serverNames, serverOnClickListener);
	    
		return builder.create();
	}
	
	class ServerOnClickListener implements DialogInterface.OnClickListener {
		
		private List<ServerInfo> servers;
		
		public void setServers(List<ServerInfo> servers) {
			this.servers = servers;
		}
		
		public void onClick(DialogInterface dialog, int index) {
			ConnectActivity.this.onServerSelected(servers.get(index));
		}
	}

	@Override
	protected void onDestroy() {
		this.unregisterReceiver(wifiMonitor);
		super.onDestroy();
	}
	
	private String getSSID() {
		String ssid = "";
		
		if(wifiManager != null) {
			WifiInfo wifiInfo = wifiManager.getConnectionInfo();

			if(wifiInfo != null) {
				ssid = wifiInfo.getSSID();
				
				if(ssid == null) {
					ssid = "";
				}
			}
		}
		
		return ssid;
	}
	
	private void setDefaultConfiguration() {
		boolean wifiOn = wifiManager.isWifiEnabled();
		if( !wifiOn ) {
			return;
		}

		DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();
		if(dhcpInfo == null) {
			return;
		}
		
		String serverAddress = null;
		final String ssid = getSSID();
			
		serverAddress = prefs.getString(ssid+"_hostname", null);
		
		if(serverAddress == null) {
			serverAddress = Utils.convertAddress(dhcpInfo.ipAddress);
			int end = serverAddress.lastIndexOf('.');
			serverAddress = serverAddress.substring(0, end+1);
		}
		
		int port = prefs.getInt(ssid + "_port", RemoteControl.DEFAULT_SERVER_PORT);
		String password = prefs.getString(ssid + "_password", "");
		
		setConfiguration(serverAddress, port, password);
		
		if(ssidTextView != null) {
			ssidTextView.setText(ssid);
		}
	}
	
	private void setConfiguration(String address, int port, String password) {
		if(address != null) {
			ipEdit.setText(address);
		}
		
		port = Utils.checkPort(port);		
		portEdit.setText(Integer.toString(port));
		
		if(password != null) {
			passEdit.setText(password);
		}
	}

	@Override
	public void onClick(View view) {
		final int id = view.getId();
		final RemoteControl remoteControl = RemoteControl.getInstance();
		remoteControl.initialize(this);
		
		switch(id) {
		case R.id.connect_button:
			onConnect();
			break;
			
		case R.id.server_detection_button:
			onSearchServer();
			break;
			
		case R.id.wol_button:
			onWakeOnLan();
			break;
			
		case R.id.ssid_button:
			onWifiSetting();
			break;
		}
	}
	
	private void onConnect() {
		Toast toast;
		String ssid = getSSID();
		String hostName = ipEdit.getEditableText().toString();
		String portNumber = portEdit.getEditableText().toString();
		int port = 0;

		final RemoteControl remoteControl = RemoteControl.getInstance();
		
		try {
			port = Integer.parseInt(portNumber);
			if(port < 1 || port > 0xFFFF) {
				throw new NumberFormatException(); 
			}
			
			InetAddress.getByName(hostName);
		} catch(NumberFormatException nfe) {
			toast = Toast.makeText(this, R.string.port_number_error, Toast.LENGTH_SHORT);
			toast.show();
			return;
		} catch (UnknownHostException e) {
			toast = Toast.makeText(this, R.string.unknown_host_error, Toast.LENGTH_SHORT);
			toast.show();
			return;
		}
		
		boolean wifiOn = wifiManager.isWifiEnabled();
		if(!wifiOn) {
			ssid = "";
		}

		String password = passEdit.getText().toString();

		// Save the latest connection information.
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(ssid+"_hostname", hostName);
		editor.putString(ssid+"_password", password);
		editor.putInt(ssid+"_port", port);			
		editor.commit();
		
		int result;
		result = remoteControl.authenticate(hostName, port, password);
		
		if(result == Result.OK) {
			final String text = getResources().getString(R.string.connection_success);
			toast = Toast.makeText(this, hostName + "\n" + text, Toast.LENGTH_SHORT);
			toast.show();
			
			byte[] mac = remoteControl.getServerMacAddress();
		    if(mac != null) {
		    	editor = prefs.edit();
		    	long macAddress = Utils.convertMacAddress(mac);
			    editor.putLong(ssid+"_mac", macAddress);
			    editor.commit();
		    }
			
			Intent intent = new Intent("CONNECTED");
			this.sendBroadcast(intent);

			finish();
		}
		else {
			int stringId;
			
			switch(result) {
			case Result.ERROR_AUTHENTICATION_FAILURE:
				stringId = R.string.authentication_failure;
				break;
			case Result.ERROR_CONNECTION_FAILURE:
			default:
				stringId = R.string.connection_failure; 
				break;
			}
			
			toast = Toast.makeText(this, stringId, Toast.LENGTH_SHORT);
			toast.show();
		}
	}
	
	private void onSearchServer() {
		serverSearcher.run();
		loadingDialog.show();
	}
	
	private void onWakeOnLan() {
		RemoteControl remoteControl = RemoteControl.getInstance();
		boolean success = remoteControl.wakeOnLan();
		int resId = success ? R.string.wake_on_lan_success : R.string.wake_on_lan_failure;
		
		Toast toast = Toast.makeText(this, resId, Toast.LENGTH_SHORT);
		toast.show();
	}
	
	private void onWifiSetting() {
		startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
	}
	
	private void completeConnectionSuccess(
			final String hostName, final int port, final String password, final String serverName) {
		String ssid = getSSID();
		final RemoteControl remoteControl = RemoteControl.getInstance();
		
		// Save the latest connection information.
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(ssid+"_hostname", hostName);
		editor.putString(ssid+"_password", password);
		editor.putInt(ssid+"_port", port);			
		
		byte[] mac = remoteControl.getServerMacAddress();
	    if(mac != null) {
	    	long macAddress = Utils.convertMacAddress(mac);
		    editor.putLong(ssid+"_mac", macAddress);
	    }
	    
		editor.commit();
		
		Intent intent = new Intent("CONNECTED");
		final String packageName = this.getPackageName();
		intent.putExtra(packageName + ".name", serverName);
		intent.putExtra(packageName + ".ip", hostName);
		
		this.sendBroadcast(intent);		
	}
	
	private void toastConnectionFailure(final int result) {
		int stringId;
		
		switch(result) {
		case Result.ERROR_AUTHENTICATION_FAILURE:
			stringId = R.string.authentication_failure;
			break;
		case Result.ERROR_CONNECTION_FAILURE:
		default:
			stringId = R.string.connection_failure; 
			break;
		}
		
		Toast toast = Toast.makeText(this, stringId, Toast.LENGTH_SHORT);
		toast.show();
	}
	
	private void toastConnectionSuccess(final ServerInfo serverInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append(serverInfo.getName());
		sb.append('\n');
		sb.append(Utils.convertAddress(serverInfo.getIp()));
		sb.append("\n");
		sb.append( getResources().getString(R.string.connection_success) );
		
		Toast toast = Toast.makeText(this, sb.toString(), Toast.LENGTH_LONG);
		toast.show();
	}
	
	public void updateWifiStatus() {
		final String ssid = getSSID();
		ssidTextView.setText(ssid);
	}

	class WifiMonitor extends BroadcastReceiver {
		
		private ConnectActivity parent;

		public WifiMonitor(ConnectActivity parent) {
			this.parent = parent;
		}
		
		@Override
		public void onReceive(Context context, Intent intent) {
			parent.updateWifiStatus();
		}
	}

	/**
	 * ServerSearchTask.onPostExecute() calls this listener method in .
	 */
	@Override
	public void onSearchResult(List<ServerInfo> servers) {
		loadingDialog.cancel();
		
		final int size = servers.size();
		
		if(size == 0) {
			Toast toast = Toast.makeText(this, R.string.detection_failure, Toast.LENGTH_SHORT);
			toast.show();
		}
		else if(size == 1) {
			onServerSelected(servers.get(0));
		}
		else {
			Dialog selectionDialog = this.createServerSelectionDialog(servers);
			selectionDialog.show();
		}
	}
	
	/**
	 * Server is selected.
	 * @param serverInfo
	 */
	public void onServerSelected(final ServerInfo serverInfo) {
		final RemoteControl remoteControl = RemoteControl.getInstance();

		final byte[] ip = serverInfo.getIp();
		final int port = serverInfo.getPort();
		final String password = "";
		final String serverName = serverInfo.getName();
		
		final int result = remoteControl.authenticate(ip, port, password);
		
		if(result == Result.OK) {
			completeConnectionSuccess(Utils.convertAddress(ip), port, password, serverName);
			toastConnectionSuccess(serverInfo);
			finish();
		}
		else {
			toastConnectionFailure(result);
			setConfiguration(Utils.convertAddress(ip), port, password);
		}
	}
}