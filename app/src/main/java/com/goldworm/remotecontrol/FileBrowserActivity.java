package com.goldworm.remotecontrol;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import com.goldworm.net.RemoteControl;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class FileBrowserActivity extends Activity
	implements OnItemClickListener, OnClickListener {

	private RemoteControl remoteControl;
	private LinkedList<FileInfo> fileList;
	private ListView lvFile;
	private FileListAdapter adapter;
	private FileInfoComparator comparator;
	private TextView pathTextView;
	private String cwd;
	private StringBuilder sb = new StringBuilder();
	private int appID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.filebrowser);
		
		remoteControl = RemoteControl.getInstance();
		remoteControl.initialize(this);
		
		fileList = new LinkedList<FileInfo> ();
		
		Intent intent = getIntent();
		String pathname = intent.getStringExtra("cwd");
		appID = intent.getIntExtra("appID", -1);
		
		lvFile = (ListView) this.findViewById(R.id.file_listview);
		
		int buttonIds[] = {
			R.id.close_button,
			R.id.root_button,
			R.id.up_button,
			R.id.refresh_button,
			R.id.close_button2,
		};
		Button button;
		
		int size = buttonIds.length;
		for(int i=0; i<size; i++) {
			button = (Button) this.findViewById(buttonIds[i]);
			button.setOnClickListener(this);
		}
		
//		// Version-specific code
//		if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.ECLAIR_MR1) {
//			View v = this.findViewById(R.id.home_button);
//			if(v != null) {
//				v.setEnabled(false);
//			}
//			
//			v = this.findViewById(R.id.end_button);
//			if(v != null) {
//				v.setEnabled(false);
//			}
//		}
		
		comparator = new FileInfoComparator();
		adapter = new FileListAdapter(this, R.layout.filebrowser_row);
		
		lvFile.setAdapter(adapter);
		lvFile.setOnItemClickListener(this);
		
		pathTextView = (TextView) this.findViewById(R.id.path_textview);
		
		this.cwd = pathname;
		refreshFileList(pathname);
	}
	
	private void setCWD(String cwd)
	{
		if(cwd == null) {
			return;
		}

		if(pathTextView != null) {
			pathTextView.setText(cwd);
			fullScrollCWD();
		}
		
		this.cwd = cwd;
	}
	
	private void fullScrollCWD() {
		final HorizontalScrollView scrollView =
				(HorizontalScrollView) this.findViewById(R.id.path_scrollview);
		scrollView.post(new Runnable() {
				@Override
				public void run() {
					scrollView.fullScroll(ScrollView.FOCUS_RIGHT);
				}
			});
	}
	
	private void refreshFileList(String pathname) {
		int count = remoteControl.list(pathname, fileList);
		
		if(count > -1) {
			
			adapter.clear();
			adapter.addAllContents(fileList);
			adapter.sort(comparator);
			adapter.notifyDataSetChanged();

			lvFile.setSelection(0);
			
			setCWD(pathname);
		}
		else {
			remoteControl.createSocket();
			
			Toast toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
			toast.setText(R.string.get_filelist_failure);
			toast.show();
		}
	}
	
	private void cdToParent() {
		if(cwd == null) {
			return;
		}
		
		if(cwd.length() == 0) {
			return;
		}
		
		String pathname = "";
		
		int end = cwd.lastIndexOf("\\");
		if(end > 0) {
			pathname = cwd.substring(0, end);
		}
		
		refreshFileList(pathname);
	}

	@Override
	public void onClick(View v) {
		
		int id = v.getId();
		
		switch(id) {
		case R.id.close_button:
		case R.id.close_button2:
			finish();
			break;
		case R.id.root_button:
			refreshFileList("");
			break;
		case R.id.up_button:
			cdToParent();
			break;
		case R.id.refresh_button:
			refreshFileList(this.cwd);
			break;
		}
	}
	
	@Override
	public void onBackPressed() {
		cdToParent();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View row, int position, long id) {
//		ViewWrapper wrapper = (ViewWrapper) row.getTag();
		FileInfo fileInfo = adapter.getItem(position);

		String pathname;
		String name = fileInfo.getName();
		int type = fileInfo.getType();
		
		if(type == FileInfo.DRIVE) {
			pathname = name;
		}
		else {
			sb.setLength(0);
			sb.append(cwd);
			sb.append('\\');
			sb.append(name);
			pathname = sb.toString();
		}

		if(type == FileInfo.FILE) {
			openFile(pathname);
			closeFileBrowserActivity();
		}
		else {
			refreshFileList(pathname);
		}
	}
	
	private void openFile(String pathname) {
		remoteControl.open(appID, pathname);
	}
	
	private void closeFileBrowserActivity() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
	    final boolean closeMode = prefs.getBoolean("filebrowser_close_mode", false);
	    
	    if(closeMode) {
	    	finish();
	    }
	}

	private class ViewWrapper {
		View row;
//		ImageView icon = null;
		TextView label = null;
		
		ViewWrapper(View row) {
			this.row = row;
		}
		
		TextView getLabel() {
			if(label == null) {
				label = (TextView) row.findViewById(R.id.label);
			}
			
			return label;
		}
		
//		ImageView getIcon() {
//			if(icon == null) {
//				icon = (ImageView) row.findViewById(R.id.icon);
//			}
//			
//			return icon;
//		}
	}
	
	private class FileListAdapter extends ArrayAdapter<FileInfo> {
		private Activity context;
		
		public FileListAdapter(Activity context, int textViewResourceId,
				List<FileInfo> objects) {
			super(context, textViewResourceId, objects);
			
			this.context = context;
		}
		
		public FileListAdapter(Activity context, int textViewResourceId) {
			super(context, textViewResourceId);
			
			this.context = context;
		}
		
		public void addAllContents(Collection<FileInfo> collection) {
			FileInfo fileInfo = null;
			Iterator<FileInfo> itr = collection.iterator();
			
			while(itr.hasNext()) {
				fileInfo = itr.next();
				adapter.add(fileInfo);
			}
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View row = convertView;
			ViewWrapper wrapper;
			
			if(row == null) {
				LayoutInflater inflater = context.getLayoutInflater();
				
				row = inflater.inflate(R.layout.filebrowser_row, null);
				wrapper = new ViewWrapper(row);
				row.setTag(wrapper);
			}
			else {
				wrapper = (ViewWrapper) row.getTag();
			}
			
			TextView label = wrapper.getLabel();
//			ImageView icon = wrapper.getIcon();
//			Drawable drawable = null;
			
			FileInfo fileInfo = this.getItem(position);
			final int type = fileInfo.getType();
			String longName = fileInfo.getLongName();
			int textColor = Constant.FILE_TEXT_COLOR;
			
			switch(type) {
			case FileInfo.DIRECTORY:
				longName += "/";
				textColor = Constant.DIRECTORY_TEXT_COLOR;
				break;
			case FileInfo.DRIVE:
				textColor = Constant.DRIVE_TEXT_COLOR;
//				name += ":";
				break;
			case FileInfo.FILE:
				break;
			}
			
//			if(drawable != null) {
//				icon.setImageDrawable(drawable);
//			}
			
			label.setTextColor(textColor);
			label.setText(longName);
			
			return row;
		}
	}
	
	class FileInfoComparator implements Comparator<FileInfo> {

		@Override
		public int compare(FileInfo arg0, FileInfo arg1) {
			
			int[] type = new int [2];
			type[0] = arg0.getType();
			type[1] = arg1.getType();
			
			// DIRECTORY, DRIVE, FILE
			int[] typeOrder = new int [2];
			typeOrder[0] = FileInfo.getTypeOrder(type[0]);
			typeOrder[1] = FileInfo.getTypeOrder(type[1]);
			
			if(typeOrder[0] < typeOrder[1]) {
				return -1;
			}
			
			if(typeOrder[0] > typeOrder[1]) {
				return 1;
			}
			
			String[] name = new String [2];
			name[0] = arg0.getName().toLowerCase(Locale.ENGLISH);
			name[1] = arg1.getName().toLowerCase(Locale.ENGLISH);
			
			return name[0].compareTo(name[1]);
		}
	}
}
