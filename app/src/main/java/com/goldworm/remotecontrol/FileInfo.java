package com.goldworm.remotecontrol;

public class FileInfo {
	
	public static final int DIRECTORY = 0;
	public static final int DRIVE = 1;
	public static final int FILE = 2;
	
	public static final int typeOrder[] = { 1, 0, 2 };
	
	/**
	 * DIRECTORY, DRIVE, FILE 
	 */
	private int type;
	
	/**
	 * Not absolute pathname.
	 * Real pathname
	 */
	private String name;
	
	/**
	 * Display name
	 */
	private String longName;
	
	public FileInfo(final int type, final String name, final String longName) {
		this.type = type;
		this.name = name;
		this.longName = longName;
	}
	
	public static int getTypeOrder(int type) {
		return typeOrder[type];
	}

	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Drive only
	 * @param longName
	 */
	public void setLongName(String longName) {
		this.longName = longName;
	}
	/**
	 * Drive only
	 * @return
	 */
	public String getLongName() {
		return longName;
	}
}
