package com.goldworm.remotecontrol;
import com.goldworm.net.RemoteControl;

import android.view.View;
import android.view.View.OnClickListener;


public class KeyPadListener implements OnClickListener {

	private final int[] viewIds;
	private final int[] keyCodes;
	protected RemoteControl remoteControl;
	
	public KeyPadListener(RemoteControl remoteControl, final int[] ids, final int[] keyCodes) {
		viewIds = ids;
		this.keyCodes = keyCodes;
		this.remoteControl = remoteControl;
	}

	public void setOnClickListener(View viewGroup) {
		int length = viewIds.length;
		int i;
		View view;
		
		for(i=0; i<length; i++) {
			view = viewGroup.findViewById(viewIds[i]);
			if(view != null) {
				view.setOnClickListener(this);
			}
		}
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		
		int length = viewIds.length;
		int i;
		
		for(i=0; i<length; i++) {
			if(id == viewIds[i]) {
				remoteControl.controlKeyboard(keyCodes[i]);
				break;
			}
		}
	}
}
