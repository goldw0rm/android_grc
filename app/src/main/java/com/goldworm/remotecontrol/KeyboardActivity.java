package com.goldworm.remotecontrol;

import com.goldworm.fragment.KeyboardFragment;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;

public class KeyboardActivity extends FragmentActivity {

	private KeyboardFragment keyboardFragment;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.keyboard_land);
		
		keyboardFragment = (KeyboardFragment) getSupportFragmentManager().findFragmentById(R.id.keyboard);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		if(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			this.finish();
		}

		super.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if( keyboardFragment.onKeyDown(keyCode, event) ) {
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
}
