package com.goldworm.remotecontrol;

import java.util.TimerTask;

import com.goldworm.net.RemoteControlThread;

public class KeyboardPressTimerTask extends TimerTask {
	private RemoteControlThread remoteControl;
	private int count;
	private int[] keyCodes;

	public KeyboardPressTimerTask(RemoteControlThread remoteControl, int count, int[] keyCodes) {
		this.count = count;
		this.keyCodes = keyCodes;
		this.remoteControl = remoteControl;
	}
	
	@Override
	public void run() {
		remoteControl.controlKeyboard(count, keyCodes);
	}
}