package com.goldworm.remotecontrol;

import java.util.Timer;
import java.util.TimerTask;

import com.goldworm.net.RemoteControl;

import android.util.Log;

public class LongPressManager {
	
	private static final int KEY_DOWN			= 0x01000000;
	private static final int KEY_UP				= 0x02000000;
	
	private int firstPressDelay = 500;
	private int pressPeriod = 100;
	
	private Timer timer;
	
	private KeyboardPressTimerTask keyboardTimerTask;
	private ShortcutPressTimerTask shortcutTimerTask;
	
	private static final int READY = 0;
	private static final int RUNNING = 1;
	private int state;

	private RemoteControl remoteControl;
	
	public LongPressManager() {
	}
	
	public LongPressManager(int firstPressDelay, int pressPeriod) {
		this.firstPressDelay = firstPressDelay;
		this.pressPeriod = pressPeriod;
	}
	
	public void open(RemoteControl remoteControl) {
		this.remoteControl = remoteControl;
		timer = new Timer();

		state = READY;
	}
	
	public void startKeyboard(int count, int[] keyCodes) {

		if(state != READY) {
			Log.e("ERROR", "startKeyboard() is already called.");
			return;
		}
		
		remoteControl.controlKeyboard(count, keyCodes);
		
		keyboardTimerTask = new KeyboardPressTimerTask(true, count, keyCodes);
		timer.schedule(keyboardTimerTask, firstPressDelay);
		
		state = RUNNING;
	}

	public void stopKeyboard() {
		state = READY;

		if(keyboardTimerTask == null) {
			return;
		}
		
//		int count = keyboardTimerTask.getKeyCount();
//		int keyCode = keyboardTimerTask.getKeyCode();
		
		keyboardTimerTask.cancel();
		keyboardTimerTask = null;

//		remoteControl.controlKeyboard(keyCode | KEY_UP);
	}
	
	public void startShortcut(int appID, int shortcut) {
		if(state != READY) {
			Log.e("ERROR", "startShortcut() is already called.");
			return;
		}

		remoteControl.controlShortcut(appID, KEY_DOWN, shortcut);
		
		shortcutTimerTask = new ShortcutPressTimerTask(true, appID, shortcut);
		timer.schedule(shortcutTimerTask, firstPressDelay);
		
		state = RUNNING;
	}
	
	public void stopShortcut() {
		state = READY;
		
		if(shortcutTimerTask == null) {
			return;
		}
		
//		int appID = shortcutTimerTask.getAppID();
//		int shortcut = shortcutTimerTask.getShortcut();
		
		shortcutTimerTask.cancel();
		shortcutTimerTask = null;
		
//		remoteControl.controlApp(appID, shortcut | KEY_UP);
	}

	public void close() {
		timer.cancel();

		timer = null;
		remoteControl = null;
		keyboardTimerTask = null;
		shortcutTimerTask = null;
	}

	private class KeyboardPressTimerTask extends TimerTask {
		private boolean first;
		private int count;
		private int[] keyCodes;

		public KeyboardPressTimerTask(boolean first, int count, int[] keyCodes) {
			super();
			this.first = first;
			this.count = count;
			this.keyCodes = keyCodes;
		}
		
		public int getKeyCount() {
			return count;
		}
		
		public int[] getKeyCodes() {
			return keyCodes;
		}
		
		@Override
		public void run() {
			int period = LongPressManager.this.pressPeriod;
			
			remoteControl.controlKeyboard(count, keyCodes);
			
			if(first) {
				KeyboardPressTimerTask task = new KeyboardPressTimerTask(false, count, keyCodes);
				LongPressManager.this.timer.schedule(task, period, period);

				keyboardTimerTask = task;
			}
		}
	};
	
	private class ShortcutPressTimerTask extends TimerTask {

		private boolean first;
		private int appID;
		private int shortcut;
		
		public ShortcutPressTimerTask(boolean first, int appID, int shortcut) {
			super();

			this.first = first;
			this.appID = appID;
			this.shortcut = shortcut;
		}
		
		public int getAppID() {
			return appID;
		}
		
		public int getShortcut() {
			return shortcut;
		}

		@Override
		public void run() {
			int period = LongPressManager.this.pressPeriod;
			
			remoteControl.controlShortcut(appID, KEY_DOWN, shortcut);
			
			if(first) {
				ShortcutPressTimerTask task = new ShortcutPressTimerTask(false, appID, shortcut);
				LongPressManager.this.timer.schedule(task, period, period);

				shortcutTimerTask = task;
			}
		}
	}
}
