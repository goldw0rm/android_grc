/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.goldworm.remotecontrol;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TabHost;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

import com.goldworm.dialog.InfoDialog;
import com.goldworm.fragment.*;
import com.goldworm.net.RemoteControl;
import com.goldworm.net.RemoteControlThread;
import com.goldworm.utils.Utils;

public class MainActivity extends FragmentActivity implements OnSharedPreferenceChangeListener {
	
	private static final String TAG = "MAIN";
	
	private boolean first = true;
	private TabHost mTabHost;
	private TabManager mTabManager;
	private RemoteControl remoteControl;
	private SharedPreferences prefs;
	private ServerDetector serverDetector;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

//        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//        StrictMode.setThreadPolicy(policy);
        
        initDefaultParameters();
        initRemoteControl();
        initTabs(savedInstanceState);
        initServerDetector();

        setKeepScreenOn();
    }
    
    private void setKeepScreenOn() {
        final boolean keepScreenOn = prefs.getBoolean("keep_screen_on_mode", false);
        
        if(keepScreenOn) {
			// Block the screen from being turned off . 
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
        else {
        	getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }
    
    private void initDefaultParameters() {
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.registerOnSharedPreferenceChangeListener(this);
    }
    
    private void initRemoteControl() {
        remoteControl = RemoteControl.getInstance();
        remoteControl.initialize(this);
        
        RemoteControlThread thread = RemoteControlThread.getInstance();
        thread.start();
    }
    
    private void initServerDetector() {
    	serverDetector = ServerDetector.getInstance();
    	serverDetector.initialize(this);
    	serverDetector.run();
    }
    
    private void initTabs(Bundle savedInstanceState) {
    	
		final Class<?>[] classes = {
			KeyboardFragment.class,
			ShortcutFragment.class,
			MouseFragment.class,
			SystemFragment.class,
		};
			
			final String[] tags = {
				"keyboard",
				"shortcut",
				"mouse",
				"system",
			};
			
			final int[] nameIds = {
				R.string.keyboard,
				R.string.shortcut,
				R.string.mouse,
				R.string.system,
			};
			
        mTabHost = (TabHost)findViewById(R.id.tabHost);
        mTabHost.setup();

        mTabManager = new TabManager(this, mTabHost, R.id.realTabContent);

        String name;
        Resources res = getResources();
        int size = classes.length;
        
        for(int i=0; i<size; i++) {
        	name = res.getString(nameIds[i]);
	        mTabManager.addTab(
	        	mTabHost.newTabSpec(tags[i]).setIndicator(name), classes[i], null);
        }
        
	    int tab = Utils.getIntFromPrefs(prefs, "initial_tab", Constant.INITIAL_TAB_DEFAULT);  
	    mTabHost.setCurrentTab(tab);
	}
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("tab", mTabHost.getCurrentTabTag());
    }
    
    /**
     * This is a helper class that implements a generic mechanism for
     * associating fragments with the tabs in a tab host.  It relies on a
     * trick.  Normally a tab host has a simple API for supplying a View or
     * Intent that each tab will show.  This is not sufficient for switching
     * between fragments.  So instead we make the content part of the tab host
     * 0dp high (it is not shown) and the TabManager supplies its own dummy
     * view to show as the tab content.  It listens to changes in tabs, and takes
     * care of switch to the correct fragment shown in a separate content area
     * whenever the selected tab changes.
     */
    public static class TabManager implements TabHost.OnTabChangeListener {
        private final FragmentActivity mActivity;
        private final TabHost mTabHost;
        private final int mContainerId;
        private final HashMap<String, TabInfo> mTabs = new HashMap<String, TabInfo>();
        TabInfo mLastTab;

        static final class TabInfo {
            private final String tag;
            private final Class<?> clss;
            private final Bundle args;
            private Fragment fragment;

            TabInfo(String _tag, Class<?> _class, Bundle _args) {
                tag = _tag;
                clss = _class;
                args = _args;
            }
        }

        static class DummyTabFactory implements TabHost.TabContentFactory {
            private final Context mContext;

            public DummyTabFactory(Context context) {
                mContext = context;
            }

            @Override
            public View createTabContent(String tag) {
                View v = new View(mContext);
                v.setMinimumWidth(0);
                v.setMinimumHeight(0);
                v.setFocusable(false);
                v.setFocusableInTouchMode(false);
                return v;
            }
        }

        public TabManager(FragmentActivity activity, TabHost tabHost, int containerId) {
            mActivity = activity;
            mTabHost = tabHost;
            mContainerId = containerId;
            mTabHost.setOnTabChangedListener(this);
        }

        public void addTab(TabHost.TabSpec tabSpec, Class<?> clss, Bundle args) {
            tabSpec.setContent(new DummyTabFactory(mActivity));
            String tag = tabSpec.getTag();

            TabInfo info = new TabInfo(tag, clss, args);

            // Check to see if we already have a fragment for this tab, probably
            // from a previously saved state.  If so, deactivate it, because our
            // initial state is that a tab isn't shown.
            info.fragment = mActivity.getSupportFragmentManager().findFragmentByTag(tag);
            if (info.fragment != null && !info.fragment.isDetached()) {
                FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();
                ft.detach(info.fragment);
                ft.commit();
            }

            mTabs.put(tag, info);
            mTabHost.addTab(tabSpec);
        }

        @Override
        public void onTabChanged(String tabId) {
            TabInfo newTab = mTabs.get(tabId);
            if (mLastTab != newTab) {
                FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();
                if (mLastTab != null) {
                    if (mLastTab.fragment != null) {
                        ft.detach(mLastTab.fragment);
                    }
                }
                if (newTab != null) {
                    if (newTab.fragment == null) {
                        newTab.fragment = Fragment.instantiate(mActivity,
                                newTab.clss.getName(), newTab.args);
                        ft.add(mContainerId, newTab.fragment, newTab.tag);
                    } else {
                        ft.attach(newTab.fragment);
                    }
                }

                mLastTab = newTab;
                ft.commit();
                mActivity.getSupportFragmentManager().executePendingTransactions();
            }
        }
        
        public Fragment getCurrentFragment() {
        	if(mLastTab == null) {
        		Log.e(TAG, "mLastTab is null");
        		return null;
        	}
        	
        	return mLastTab.fragment;
        }
    }

    @Override
	protected void onStart() {
		super.onStart();
		
		if(first) {
			setOrientation();
			first = false;
		}
	}
    
    private void setOrientation() {
    	final boolean on = prefs.getBoolean("landscape_mode", false);
    	
		final int requestedOrientation = on ?
				ActivityInfo.SCREEN_ORIENTATION_SENSOR :
				ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
		setRequestedOrientation(requestedOrientation);    	
    }

	@Override
	protected void onDestroy() {
		RemoteControlThread thread = RemoteControlThread.getInstance();
		thread.quit();
		
		remoteControl.deinitialize();
		super.onDestroy();
	}
    
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		
		if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			Intent intent = new Intent(this, KeyboardActivity.class);
			startActivity(intent);
		}

		super.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent = null;
        int id = item.getItemId();

        switch(id) {
        case R.id.connect:
            intent = new Intent(this, ConnectActivity.class);
            startActivityForResult(intent, 0);
            break;
        case R.id.setting:
            intent = new Intent(this, SettingsActivity.class);
            startActivityForResult(intent, 0);
            break;
        case R.id.info:
            new InfoDialog(this).show();
            break;
        default:
            return false;
        }

        return true;
    }

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		Fragment fragment = mTabManager.getCurrentFragment();

		try {
			Method method = fragment.getClass().getMethod("onKeyDown", int.class, KeyEvent.class);
			
			if(method.getReturnType() == boolean.class)
			{
				Boolean result = (Boolean) method.invoke(fragment, keyCode, event);
				if( result.booleanValue() ) {
					return true;
				}
			}
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}		
		
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
		if( key.equals("landscape_mode") ) {
			setOrientation();
		}
		else if( key.equals("keep_screen_on_mode") ) {
			setKeepScreenOn();
		}
	}
}