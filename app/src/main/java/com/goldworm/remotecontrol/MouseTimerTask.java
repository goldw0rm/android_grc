package com.goldworm.remotecontrol;

import java.util.TimerTask;

import com.goldworm.net.RemoteControlThread;

public class MouseTimerTask extends TimerTask {
	private int y;
	private int action;
	private RemoteControlThread remoteControl;
	
	public MouseTimerTask(RemoteControlThread remoteControl, int action, int y) {
		super();
		this.remoteControl = remoteControl;
		this.action = action;
		this.y = y;
	}

	@Override
	public void run() {
		remoteControl.controlMouse(action, 0, y);
	}		
}

