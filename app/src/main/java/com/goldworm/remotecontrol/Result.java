package com.goldworm.remotecontrol;

public class Result {
	public static final int OK = 0;
	public static final int READY_TO_CONNECT_TO_SERVER = 1;
	
	public static final int ERROR_UNKNOWN = -100;
	public static final int ERROR_AUTHENTICATION_FAILURE = -1;
	public static final int ERROR_CONNECTION_FAILURE = -2;
	public static final int ERROR_INVALID_PORT = -3;
	public static final int ERROR_UNKNOWN_HOST = -4;
	public static final int ERROR_SOCKET = -5;
	public static final int ERROR_INVALID_ARGUMENT = -6;
}
