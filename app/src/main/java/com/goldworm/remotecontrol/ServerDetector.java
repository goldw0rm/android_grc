package com.goldworm.remotecontrol;

import java.util.ArrayList;

import com.goldworm.net.RemoteControl;
import com.goldworm.net.ServerInfo;
import com.goldworm.utils.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

/**
 * First of all try to connect the server using the previous server information.
 * If it is failed, try to detect servers in the same lan with broadcast packets.
 */
public class ServerDetector {

	private static final String TAG = "DETECT";
	private static final int MSG_SERVER_AUTHENTICATION_RESULT = 0;
	private static final int MSG_SERVER_DETECTION = 1;

	private static final int DIALOG_SERVER_SELECTION = 0;
	private static final int DIALOG_LOADING = 1;
	
	static ServerDetector serverDetector;

	boolean wifiOn;
	String ssid;
	String serverAddress;

	Activity activity;
	RemoteControl remoteControl;
	private ArrayList<ServerInfo> serverList;
	SharedPreferences prefs;
	Dialog serverSelectionDialog;
	Dialog loadingDialog;
	
	private final Handler handler = new Handler() {

		@Override
        public void handleMessage(Message msg) {
			if(msg.what == MSG_SERVER_AUTHENTICATION_RESULT) {
				// server authentication is done. The result is in msg.arg1;
				int result = msg.arg1;
				showConnectionResultToast(serverAddress, result);

				if (result == Result.OK) {
					onConnectionSuccess();
				} else {
					onConnectionFailure();
				}
			}
        	if(msg.what == MSG_SERVER_DETECTION) {
				ServerDetector serverDetector = (ServerDetector) msg.obj;
				if(serverDetector == null) {
					Log.e(TAG, "ServerDetector is null.");
					return;
				}

				try {
        			serverDetector.hideDialog(DIALOG_LOADING);
        		} catch(Exception e) {
        		}

        		int size = msg.arg1;
        		if(size == 1) {
        			serverDetector.connectToServerAndSaveServerInfo(0);
        		}
        		else if(size > 1) {
        			serverDetector.showDialog(DIALOG_SERVER_SELECTION);
        		}
        		else {
        			// Failed to detect a server. 
        			Toast toast = Toast.makeText(serverDetector.activity, "", Toast.LENGTH_SHORT);
        			toast.setText(R.string.detection_failure);
        			toast.show();
        			
        			serverDetector.moveToConnectActivity(
        				null, RemoteControl.DEFAULT_SERVER_PORT, null);
        		}
        	}
        }
	};
	
	private ServerDetector() {
		super();
	}

	public static ServerDetector getInstance() {
		if(serverDetector == null) {
			serverDetector = new ServerDetector();
		}
		
		return serverDetector;
	}
	
	public void initialize(Activity activity) {
		this.activity = activity;
		this.remoteControl = RemoteControl.getInstance();
		serverList = new ArrayList<ServerInfo> (10);
		prefs = PreferenceManager.getDefaultSharedPreferences(activity);
		
		serverSelectionDialog = createServerSelectionDialog();
		loadingDialog = createLoadingDialog();		
	}
	
	/**
	 * Check for wifi's status and try to detect and connect to a server.
	 */
	public void run() {
		int result = Result.ERROR_UNKNOWN;

	    wifiOn = isWifiOn();
		ssid = getSSID(wifiOn);
		serverAddress = getHostName(ssid);

		if(!wifiOn) {
			// Move to connectActivity to get server information.
			moveToConnectActivity(serverAddress, RemoteControl.DEFAULT_SERVER_PORT, null);
			return;
		}
	    
	    if(serverAddress != null) {
			// Firstly try to connect to a server with saved information.
			byte[] ip = Utils.getIpByName(serverAddress);

			if (ip != null) {
				final String password = prefs.getString(ssid + "_password", null);
				final int port = prefs.getInt(ssid + "_port", RemoteControl.DEFAULT_SERVER_PORT);

				result = connectToServer(ip, port, password);
				if (result == Result.READY_TO_CONNECT_TO_SERVER) {
					return;
				}
			}
		}

		Message msg = handler.obtainMessage(
				MSG_SERVER_AUTHENTICATION_RESULT, Result.ERROR_UNKNOWN, -1);
		handler.sendMessage(msg);
	}

	/**
	 * Assume that wifi is on.
	 * Server connection and authentication are failed.
	 */
	private void onConnectionFailure() {
		showDialog(DIALOG_LOADING);

		// detecting servers on another thread.
		Thread thread = new ServerDetectThread(handler, remoteControl);
		thread.start();
	}

	/**
	 * Assume that wifi is on.
	 * Server connection and authentication are succeeded.
	 */
	private void onConnectionSuccess() {
		if(ssid == null) {
			ssid = "";
		}

		// It announces that remoteControl gets ready to other activities,
		// so that they can complete to initialize.
		Intent intent = new Intent("CONNECTED");
		activity.sendBroadcast(intent);
		int stringId = R.string.connection_success;

		SharedPreferences.Editor editor = prefs.edit();

		byte[] mac = remoteControl.getServerMacAddress();
		if (mac != null) {
			long macAddress = Utils.convertMacAddress(mac);
			editor.putLong(ssid + "_mac", macAddress);
		}

		editor.commit();
	}

	private boolean isWifiOn() {
	    WifiManager wifiManager =
		    	(WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
	    
	    if(wifiManager != null) {
	    	return wifiManager.isWifiEnabled();
	    }
	    
	    return false;
	}

	
	private String getSSID(boolean wifiOn) {
	    String ssid = null;
	    if(wifiOn) {
	    	ssid = remoteControl.getSSID();
	    }
	    
	    if(ssid == null) {
	    	ssid = "";
	    }
	    
	    return ssid;
	}
	
	private String getHostName(final String ssid) {
    	String hostName = prefs.getString(ssid + "_hostname", null);
    	if(hostName == null) {
    		hostName = prefs.getString(ssid, null);
    	}

    	return hostName;
	}
	
	/**
	 * It is called to connect to a server after detecting.
	 * @param index: server's index
	 * @return
	 */
	private int connectToServerAndSaveServerInfo(final int index) {
		ServerInfo serverInfo = serverList.get(index);
		Log.v(TAG, "Name: " + serverInfo.getName());
		
		byte[] ip = serverInfo.getIp();
		int port = serverInfo.getPort();
		
		// This method shows a toast message.
		final String password = null;
		int result = connectToServer(ip, port, password);

	    String address = Utils.convertAddress(ip);

		if(result == Result.OK) {
		    String ssid = remoteControl.getSSID();
		    byte[] mac = remoteControl.getServerMacAddress();
		    
		    saveConnectedServerInfo(ssid, address, port, mac);
		}
		else {
			moveToConnectActivity(address, port, null);
		}
		
		return result;
	}
	
	/**
	 * 
	 * @param ip
	 * @param port
	 * @param password The password which is set on GoldwormServer
	 * @return
	 */
	private int connectToServer(final byte[] ip, final int port, final String password) {
		
		if(ip == null || ip.length != 4) {
			return Result.ERROR_INVALID_ARGUMENT;
		}

		new Thread(new Runnable() {
			@Override
			public void run() {
				int result = remoteControl.authenticate(ip, port, password);

				Message msg = handler.obtainMessage(
						MSG_SERVER_AUTHENTICATION_RESULT, result, -1);
				handler.sendMessage(msg);
			}
		}).start();

		return Result.READY_TO_CONNECT_TO_SERVER;
	}

	private void showConnectionResultToast(String serverAddress, int result) {
		int stringId;

		switch (result) {
			case Result.OK:
				stringId = R.string.connection_success;
				break;

			case Result.ERROR_AUTHENTICATION_FAILURE:
				stringId = R.string.authentication_failure;
				break;

			default:
				stringId = R.string.connection_failure;
				break;
		}

		Resources res = activity.getResources();
		String text = res.getString(stringId);
		String msg = serverAddress + "\n" + text;
		Toast toast = Toast.makeText(activity, msg, Toast.LENGTH_SHORT);
		toast.show();
	}

	private void moveToConnectActivity(String address, int port, String password) {
		Intent intent = new Intent(activity, ConnectActivity.class);
		
		if(address != null) {
			intent.putExtra("address", address);
			intent.putExtra("port", port);
			intent.putExtra("password", password);
		}
		
		activity.startActivity(intent);
	}
	
	private void saveConnectedServerInfo(String ssid, String address, int port, byte[] mac) {
		if(ssid == null) {
			return;
		}
		
	    SharedPreferences.Editor editor = prefs.edit();
	    editor.putString(ssid+"_hostname", address);
	    editor.putInt(ssid+"_port", port);
	    
	    if(mac != null) {
	    	long macAddress = Utils.convertMacAddress(mac);
		    editor.putLong(ssid+"_mac", macAddress);			    	
	    }
	    
	    editor.commit();
	}
	
	private void showDialog(int id) {
		Dialog dialog = null;
		
		if(id == DIALOG_SERVER_SELECTION) {
			dialog = serverSelectionDialog;
		}
		else if(id == DIALOG_LOADING) {
			dialog = loadingDialog;
		}
		
		if(dialog != null) {
			dialog.setOwnerActivity(activity);
			dialog.show();
		}
	}
	
	private void hideDialog(int id) {
		if(id == DIALOG_SERVER_SELECTION) {
			serverSelectionDialog.cancel();
		}
		else if(id == DIALOG_LOADING) {
			loadingDialog.cancel();
		}
	}
	
	public Dialog createServerSelectionDialog() {
		int i;
		int size = serverList.size();
		if(size == 0) {
			return null;
		}
		
		AlertDialog.Builder builder;
		
		ServerInfo serverInfo;
		String[] serverNames = new String [size];
		for(i=0; i<size; i++)
		{
			serverInfo = serverList.get(i);
			serverNames[i] = serverInfo.getName();
		}			

		builder = new AlertDialog.Builder(activity);
		builder.setTitle("Select a server");
	    builder.setCancelable(true);
	    builder.setItems(serverNames,
	    	new DialogInterface.OnClickListener() {
	    		public void onClick(DialogInterface dialog, int index) {
	    			connectToServerAndSaveServerInfo(index);
	    		}
	    	}
	    );

		return builder.create();
	}
	
	public Dialog createLoadingDialog() {
		Resources res = activity.getResources();
		ProgressDialog loadingDialog = new ProgressDialog(activity);
        loadingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        loadingDialog.setMessage(res.getString(R.string.detecting_server));
        loadingDialog.setCancelable(false);
	    
        return loadingDialog;		
	}
	
	private class ServerDetectThread extends Thread {
		private Handler handler;
		private RemoteControl remoteControl;
		
		ServerDetectThread(Handler handler, RemoteControl remoteControl) {
			this.handler = handler;
			this.remoteControl = remoteControl;
		}
		
		public void run() {
			int size = remoteControl.detectServer(ServerDetector.this.serverList);

			// size: the number of detected servers
			// serverList contains information on the detected server.
			Message msg = handler.obtainMessage(
				MSG_SERVER_DETECTION, size, 0, ServerDetector.this);
			handler.sendMessage(msg);
		}
	}
}
