package com.goldworm.remotecontrol;

import java.util.ArrayList;
import java.util.List;

import com.goldworm.net.RemoteControl;
import com.goldworm.net.ServerInfo;

import android.os.AsyncTask;

public class ServerSearcher {
	
	public interface OnSearchListener {
		public void onSearchResult(List<ServerInfo> servers);
	}

	private OnSearchListener listener;
	
	public void run() {
		new ServerSearchTask().execute();
	}
	
	public void setOnSearchListener(OnSearchListener listener) {
		this.listener = listener;
	}
	
	private void fireSearchEvent(List<ServerInfo> servers) { 
		if(listener != null) {
			listener.onSearchResult(servers);
		}
	}
	
	private class ServerSearchTask extends AsyncTask<Void, Void, List<ServerInfo>> {
		
		@Override
		protected List<ServerInfo> doInBackground(Void... arg0) {
			
			RemoteControl remoteControl = RemoteControl.getInstance();
			
			ArrayList<ServerInfo> servers = new ArrayList<ServerInfo> (10); 
			remoteControl.detectServer(servers);
			
			return servers;
		}

		@Override
		protected void onPostExecute(List<ServerInfo> servers) {
			// It is called in UI thread.
			ServerSearcher.this.fireSearchEvent(servers);
		}
	}
}