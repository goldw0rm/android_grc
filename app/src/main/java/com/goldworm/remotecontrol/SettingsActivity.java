package com.goldworm.remotecontrol;

import com.goldworm.utils.Utils;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class SettingsActivity extends PreferenceActivity 
	implements OnSharedPreferenceChangeListener {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		
		String key;
		Preference pref;
		SharedPreferences prefs =
			PreferenceManager.getDefaultSharedPreferences(this);
		prefs.registerOnSharedPreferenceChangeListener(this);
		
		int value;
		Resources res = this.getResources();
		
		key = "initial_tab";
		pref = findPreference(key);
		value = Utils.getIntFromPrefs(prefs, key, Constant.INITIAL_TAB_DEFAULT);
		String[] tabs = res.getStringArray(R.array.entries_initial_tab);
		pref.setSummary(tabs[value]);
		
		key = "connection_timeout";
		pref = findPreference(key);
		value = Utils.getIntGreaterThanZeroFromPrefs(prefs, key, Constant.CONNECTION_TIMEOUT_DEFAULT);
		pref.setSummary(value + "ms");
		
		key = "key_delay";
		pref = findPreference(key);
		value = Utils.getIntGreaterThanZeroFromPrefs(prefs, key, Constant.KEY_DELAY_DEFAULT);
		pref.setSummary(value + "ms");

		key = "key_period";
		pref = findPreference(key);
		value = Utils.getIntGreaterThanZeroFromPrefs(prefs, key, Constant.KEY_PERIOD_DEFAULT);
		pref.setSummary(value + "ms");
		
		key = "mousemove_threshold";
		pref = findPreference(key);
		value = Utils.getIntGreaterThanZeroFromPrefs(prefs, key, Constant.MOUSEMOVE_THRESHOLD_DEFAULT);
		pref.setSummary(value + "px");

		key = "wheel_threshold";
		pref = findPreference(key);
		value = Utils.getIntGreaterThanZeroFromPrefs(prefs, key, Constant.WHEEL_THRESHOLD_DEFAULT);
		pref.setSummary(value + "px");
		
		key = "keyboard_mode";
		pref = findPreference(key);
		value = Utils.getIntFromPrefs(prefs, key, Constant.KEYBOARD_MODE_DEFAULT);
		String[] modes = res.getStringArray(R.array.entries_keyboard_mode);
		pref.setSummary(modes[value]);
		
//		pref = findPreference("automatic_focus");
//		boolean on = prefs.getBoolean("automatic_focus", Constant.AUTOMATIC_FOCUS);		
	}

	@Override
	protected void onDestroy() {
		SharedPreferences prefs =
			PreferenceManager.getDefaultSharedPreferences(this);
		prefs.unregisterOnSharedPreferenceChangeListener(this);
		
		super.onDestroy();
	}
	
	@Override
	public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
		Preference pref = findPreference(key);
		if(pref == null) {
			return;
		}
		
		String value;

		if(key.equals("connection_timeout")) {
			value = prefs.getString(key, Constant.CONNECTION_TIMEOUT_DEFAULT);
			pref.setSummary(value+"ms");
		}
		else if(key.equals("key_delay")) {
			value = prefs.getString(key, Constant.KEY_DELAY_DEFAULT);
			pref.setSummary(value+"ms");
		}
		else if(key.equals("key_period")) {
			value = prefs.getString(key, Constant.KEY_PERIOD_DEFAULT);
			pref.setSummary(value+"ms");			
		}
		else if(key.equals("mousemove_threshold")) {
			value = prefs.getString(key, Constant.MOUSEMOVE_THRESHOLD_DEFAULT);
			pref.setSummary(value+"px");			
		}
		else if(key.equals("wheel_threshold")) {
			value = prefs.getString(key, Constant.WHEEL_THRESHOLD_DEFAULT);
			pref.setSummary(value+"px");			
		}
		else if(key.equals("initial_tab")) {
			int index = Utils.getIntFromPrefs(
				prefs, key, Constant.INITIAL_TAB_DEFAULT);
			
			Resources res = this.getResources();
			String[] tabs = res.getStringArray(R.array.entries_initial_tab);
			pref.setSummary(tabs[index]);
		}
		else if(key.equals("keyboard_mode")) {
			int index = Utils.getIntFromPrefs(
				prefs, key, Constant.KEYBOARD_MODE_DEFAULT);
			
			Resources res = this.getResources();
			String[] modes = res.getStringArray(R.array.entries_keyboard_mode);
			pref.setSummary(modes[index]);
		}
	}
}