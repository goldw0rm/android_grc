package com.goldworm.remotecontrol;

import com.goldworm.net.RemoteControl;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;

public class SpecialDialog extends Dialog implements OnClickListener {
	
	private final int[] virtualKeys = {
		VirtualKey.VK_INSERT,
		VirtualKey.VK_DELETE,
		VirtualKey.VK_BACK,
			
		VirtualKey.VK_HOME,
		VirtualKey.VK_END,
		VirtualKey.VK_ESCAPE,
		
		VirtualKey.VK_PAGEUP,
		VirtualKey.VK_PAGEDOWN,
		VirtualKey.VK_LWIN,
		
		VirtualKey.VK_ADD,
		VirtualKey.VK_SUBTRACT,
		VirtualKey.VK_TAB,
		
		VirtualKey.VK_MULTIPLY,
		VirtualKey.VK_DIVIDE,
		VirtualKey.VK_PAUSE,
	};
	
    private final int[] buttonIds = {
    	R.id.InsertButton,
    	R.id.DeleteButton,
    	R.id.BackSpaceButton,
    	
    	R.id.HomeButton,
    	R.id.EndButton,
    	R.id.EscapeButton,
    	
    	R.id.PageUpButton,
    	R.id.PageDownButton,
    	R.id.WindowButton,
    	
    	R.id.AddButton,
    	R.id.SubtractButton,
    	R.id.TabButton,

    	R.id.MultiplyButton,
    	R.id.DivideButton,
    	R.id.PauseButton,

    	R.id.CloseButton,
    };
	
	public SpecialDialog(Context context) {
		super(context);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.special);
		this.setTitle(R.string.special);
		
		int i;
		Button button;
		int size = buttonIds.length;
		
        for(i=0; i<size; i++) {
        	button = (Button) this.findViewById(buttonIds[i]);
        	button.setOnClickListener(this);
        }
	}

	@Override
	public void onClick(View view) {
		int i;
		int keyCode = -1;
		int id = view.getId();
		
		if(id == R.id.CloseButton) {
			this.cancel();
			return;
		}

		int length = buttonIds.length; 
		for(i=0; i<length; i++) {
			if(id == buttonIds[i]) {
				keyCode = virtualKeys[i];
				break;
			}
		}
		
		RemoteControl remoteControl = RemoteControl.getInstance();
		remoteControl.controlKeyboard(keyCode);
	}
}