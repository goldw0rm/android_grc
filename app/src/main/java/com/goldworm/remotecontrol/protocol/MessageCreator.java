package com.goldworm.remotecontrol.protocol;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.goldworm.net.RemoteControl;
import com.goldworm.utils.Utils;

public class MessageCreator {
	
	private int session;
	private ByteBuffer byteBuffer;
	
	public MessageCreator(final int capacity) {
		byteBuffer = ByteBuffer.allocate(capacity);
		byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
	}
	
	public final ByteBuffer getByteBuffer() {
		return byteBuffer;
	}
	
	private void fillMessageHeader(final int command, final int size) {
		byteBuffer.clear();

		byteBuffer.putInt(command); // Type(4)
		byteBuffer.putInt(size); // Length(4)
		byteBuffer.putInt(session); // Session(4)
	}
	
	private void fillMessageHeaderIncludingSubType(final int command, final int size, final int subType) {
		fillMessageHeader(command, size);
		byteBuffer.putInt(subType); // SubType(4)
	}
	
	public final void setSession(final int session) {
		this.session = session;
	}
	
	public final ByteBuffer getServerDetectionData() {
		final int size = 16;

		fillMessageHeaderIncludingSubType(
				RemoteControl.CMD_SERVER, size, RemoteControl.SERVER_DETECTION);
		
		return byteBuffer;
	}
	
	/*
	 * Request=Type(4):Length(4):Session(4):SubType(4):MD5(16)
	 * Response=Type(4):Length(4):Session(4):SubType(4):Result(4):Mac(6)
	 */
	public final ByteBuffer getServerAuthenticateData(final String password) {
		final int size = 32;
		
		fillMessageHeaderIncludingSubType(
				RemoteControl.CMD_SERVER, size, RemoteControl.SERVER_AUTHENTICATE);

		byte[] md5sum = Utils.getMD5Sum(password);		
		byteBuffer.put(md5sum); // password MD5 (16)
		
		return byteBuffer;
	}
	
	public final ByteBuffer getSetAppData(final int appID) {
		final int size = 12;

		byteBuffer.clear();
		byteBuffer.putInt(size);
		byteBuffer.putInt(RemoteControl.CMD_SET_APP);
		byteBuffer.putInt(appID);
		
		return byteBuffer;
	}
	
	/**
	 * ShorcutRequest=Type(4):Length(4):Session(4):SubType(4):AppID(4):KeyStatus
	 * (4):Shortcut(4)
	 * 
	 * @param appID
	 * @param action
	 */
	public final ByteBuffer getControlShortcutData(final int appID, final int keyStatus, final int action) {
		final int size = 28;

		fillMessageHeaderIncludingSubType(
				RemoteControl.CMD_SHORTCUT, size, RemoteControl.SHORTCUT_KEY);
		
		byteBuffer.putInt(appID);
		byteBuffer.putInt(keyStatus); // KeyStatus
		byteBuffer.putInt(action);
		
		return byteBuffer;
	}
	
	/**
	 * Request=Type(4):Length(4):Session(4):SubType(4):undefined
	 */
	public final ByteBuffer getControlSystemData(final int subType) {
		final int size = 16;

		fillMessageHeaderIncludingSubType(
				RemoteControl.CMD_SYSTEM_CONTROL, size, subType);

		return byteBuffer;
	}
	
	/**
	 * Request=Type(4):Length(4):Session(4):Action(4):x(4):y(4)
	 * 
	 * @param action
	 * @param x
	 * @param y
	 */
	public final ByteBuffer getControlMouseData(final int action, final int x, final int y) {
		final int size = 24;

		fillMessageHeaderIncludingSubType(RemoteControl.CMD_MOUSE, size, action);
		
		byteBuffer.putInt(x);
		byteBuffer.putInt(y);

		return byteBuffer;
	}
	
	/**
	 * Request=Type(4):Length(4):Session(4):KeyCount(4):KeyCodes(4):KeyCodes(4):
	 * ...
	 */
	public final ByteBuffer getControlKeyboardData(int count, int[] keyCodes) {
		final int size = 16 + count * (Integer.SIZE / 8);

		fillMessageHeaderIncludingSubType(RemoteControl.CMD_KEYBOARD, size, count);

		for (int i = 0; i < count; i++) {
			byteBuffer.putInt(keyCodes[i]);
		}

		return byteBuffer;
	}
	
	/**
	 * AppListRequest=Type(4):Length(4):Session(4):SubType(4)
	 * AppListResponse=Type
	 * (4):Length(4):Session(4):SubType(4):AppCount(4):TextSize
	 * (4):Text:TextSize(4):Text:TextSize(4):Text:...
	 * 
	 * @param adapter
	 * @return
	 */
	public final ByteBuffer getGetAppListData() {
		final int size = 16;

		fillMessageHeaderIncludingSubType(
				RemoteControl.CMD_SHORTCUT, size, RemoteControl.SHORTCUT_APP_LIST);
		
		return byteBuffer;
	}
	
	/**
	 * Get the title of the current focused application. 
	 * @return
	 */
	public final ByteBuffer getGetTitleData(final int appID) {
		final int size = 20;

		fillMessageHeaderIncludingSubType(
				RemoteControl.CMD_SHORTCUT, size, RemoteControl.SHORTCUT_TITLE);

		byteBuffer.putInt(appID);
		
		return byteBuffer;
	}
	
	/**
	 * Send a text in UTF-8 to the server.
	 * The server will convert the text to unicode format. 
	 * @param text
	 */
	public final ByteBuffer getSendTextData(final String text) {
		int length = text.length();
		if(length == 0) {
			return null;
		}
		
		final byte[] textData = text.getBytes();
		final int size = 24 + textData.length;
		
		fillMessageHeaderIncludingSubType(RemoteControl.CMD_TEXT, size, 0);
		
		byteBuffer.putInt(length); // textLength
		byteBuffer.putInt(textData.length);
		byteBuffer.put(text.getBytes());

		return byteBuffer;
	}
	
	/**
	 * Request=Type(4):Length(4):Session(4):SubType(4)
	 * Response=Type(4):Length(4):Session(4):SubType(4):Result(4):Volume(4):Min(4):Max(4)
	 * 
	 * @param volume
	 * @return
	 */
	public final ByteBuffer getGetVolumeData(final int[] volume) {
		final int size = 16;

		fillMessageHeaderIncludingSubType(
				RemoteControl.CMD_SYSTEM_CONTROL, size, RemoteControl.SYSTEM_GET_VOLUME);
		
		return byteBuffer;
	}
	
	/**
	 * Request=Type(4):Length(4):Session(4):SubType(4):Volume(4)
	 * 
	 * @param volume
	 */
	public final ByteBuffer getSetVolumeData(final int volume) {
		final int size = 20;

		fillMessageHeaderIncludingSubType(
				RemoteControl.CMD_SYSTEM_CONTROL, size, RemoteControl.SYSTEM_SET_VOLUME);

		byteBuffer.putInt(volume);

		return byteBuffer;
	}
	
	/**
	 * Request=Type(4):Length(4):Session(4):SubType(4):AppID(4):TextLength(4):ByteSize(4):Text(UTF-8)
	 * Response=Type(4):Length(4):Session(4):SubType(4)
	 */
	public final ByteBuffer getOpenData(final int appID, final String pathname) {
		final int textLength = pathname.length();
		byte[] data = null;

		try {
			data = pathname.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
		
		final int size = 28 + data.length; 

		fillMessageHeaderIncludingSubType(
				RemoteControl.CMD_SHORTCUT, size, RemoteControl.SHORTCUT_OPEN);
		
		byteBuffer.putInt(appID);
		byteBuffer.putInt(textLength);
		byteBuffer.putInt(data.length);
		byteBuffer.put(data);
		
		return byteBuffer;
	}
	
	/**
	 * Request=Type(4):Length(4):Session(4):SubType(4)
	 * Response=Type(4):Length(4):Session(4):SubType(4):TextSize(4):Text(UTF-8)
	 * 
	 * @param volume
	 */
	public final ByteBuffer getCwdData() {
		final int size = 16;

		fillMessageHeaderIncludingSubType(
				RemoteControl.CMD_FILE_BROWSE, size, RemoteControl.FILE_BROWSE_CWD);
		
		return byteBuffer;
	}

	public final ByteBuffer getListData(final String pathname) {
		int size = 20;
		byte[] data = null;

		if(pathname != null) {
			try {
				data = pathname.getBytes("UTF-8");
				size += data.length;				
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				return null;
			}
		}
		
		fillMessageHeaderIncludingSubType(
				RemoteControl.CMD_FILE_BROWSE, size, RemoteControl.FILE_BROWSE_LIST);
		
		if(data != null) {
			byteBuffer.putInt(data.length);
			byteBuffer.put(data);
		} else {
			byteBuffer.putInt(0);
		}
		
		return byteBuffer;
	}
	
	public final ByteBuffer getSetNonSleepingMode(final int requestedNonSleepingMode) {
		final int size = 20;

		fillMessageHeaderIncludingSubType(
				RemoteControl.CMD_SYSTEM_CONTROL, size, RemoteControl.SYSTEM_NON_SLEEPING_MODE);
		
		byteBuffer.putInt(requestedNonSleepingMode);
		
		return byteBuffer;
	}
}
