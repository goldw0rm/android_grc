package com.goldworm.ui;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;


public class PageIndicator extends View {

	Paint paint = new Paint();
	Rect rect = new Rect();
	
	public PageIndicator(Context context) {
		super(context);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		final int width = getWidth();
		final int height = getHeight();
		
		paint.setARGB(128, 0, 100, 0);
		rect.set(0, 0, width, height);
		canvas.drawRect(rect, paint);
	}
}
