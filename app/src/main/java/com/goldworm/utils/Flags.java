package com.goldworm.utils;

public class Flags {
	int flags;

	public Flags() {
		super();
		flags = 0;
	}
	
	public int getFlags() {
		return flags;
	}
	
	public int getFlags(int filter) {
		return flags & filter;
	}
	
	public int setFlags(int filter, boolean on) {
		if(on) {
			flags |= filter;
		}
		else {
			flags &= ~filter; 
		}
		
		return flags;
	}
	
	public int toggleFlags(int filter) {
		boolean on = (getFlags(filter) == filter);
		setFlags(filter, !on);
		
		return flags;
	}
}
