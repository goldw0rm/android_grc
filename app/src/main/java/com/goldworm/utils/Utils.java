package com.goldworm.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

import com.goldworm.net.RemoteControl;

public class Utils {
	public static byte[] getIpByName(String hostName) {
		if(hostName == null) {
			return null;
		}
		
		try {
			InetAddress address = InetAddress.getByName(hostName);
			byte[] ip = address.getAddress();
			
			return ip;
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static int getIntFromPrefs(
		SharedPreferences prefs, String key, String defValue) {
		int value = -1;
		
		try {
			value = Integer.parseInt(prefs.getString(key, defValue));
		}
		catch(Exception e) {
		}
		
		if(value < 0) {
			value = Integer.parseInt(defValue);			
		}

		return value;
	}
	
	public static int getIntGreaterThanZeroFromPrefs(
		SharedPreferences prefs, String key, String defValue) {
		int value = 0;
		
		try {
			value = Integer.parseInt(prefs.getString(key, defValue));
		}
		catch(Exception e) {
		}
		
		if(value < 1) {
			value = Integer.parseInt(defValue);			
		}

		return value;
	}
	
	public static String convertAddress(final byte[] ip) {
		StringBuffer sb = new StringBuffer();

		sb.append(((int) ip[0]) & 0xFF);
		sb.append('.');
		sb.append(((int) ip[1]) & 0xFF);
		sb.append('.');
		sb.append(((int) ip[2]) & 0xFF);
		sb.append('.');
		sb.append(((int) ip[3]) & 0xFF);
		
		return sb.toString();
	}
	
	public static String convertAddress(final int ip) {
		StringBuffer sb = new StringBuffer();
		sb.append(ip & 0xFF);
		sb.append('.');
		sb.append((ip & 0xFF00) >>> 8);
		sb.append('.');
		sb.append((ip & 0xFF0000) >>> 16);
		sb.append('.');
		sb.append(ip >>> 24);
		
		return sb.toString();
	}
	
	public static int checkPort(int port) {
		if(port < 1 || port > 0xFFFF) {
			port = RemoteControl.DEFAULT_SERVER_PORT;
		}
		
		return port;
	}
	
	public static boolean getBroadcastIP(int ip, int netmask, byte[] broadcastIP) {
		int broadcast = (ip & netmask) | (0xFFFFFFFF & (~netmask));
		
		broadcastIP[0] = (byte) broadcast;
		broadcastIP[1] = (byte) (broadcast >> 8);
		broadcastIP[2] = (byte) (broadcast >> 16);
		broadcastIP[3] = (byte) (broadcast >> 24);
		
		return true;
	}
	
	public static long convertMacAddress(byte[] mac) {
		int i;
		long value = mac[5];
		long macDigit;
		
		for(i=4; i>=0; i--) {
			macDigit = (long) mac[i] & 0xFFL;
			
			value <<= 8;
			value |= macDigit;
		}
		
		return value;
	}
	
	public static byte[] convertMacAddress(long mac) {
		byte[] value = new byte[6];
		
		for(int i=0; i<6; i++) {
			value[i] = (byte) (mac & 0xFF);
			mac >>= 8;
		}
		
		return value;
	}
	
	public static byte[] getMD5Sum(final String text) {
		byte[] md5sum = null;
		
		if (text != null && text.length() > 0) {
			try {
				MessageDigest digest = MessageDigest.getInstance("MD5");
				digest.update(text.getBytes());
				md5sum = digest.digest();
			} catch (NoSuchAlgorithmException e1) {
				e1.printStackTrace();
				Log.e("ERROR", "MD5 doesn't supported.");
			}
		} else {
			md5sum = new byte[16];
		}
		
		return md5sum;
	}
	
	public static String getPackageVersionName(Context context) {
		PackageManager pm = context.getPackageManager();
		try {
			return pm.getPackageInfo(context.getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		
		return "";
	}
	
	public static void printBytesToHexString(String tag, final byte[] bytes) {
		StringBuilder sb = new StringBuilder();
		int value;
		
		for(int i=0; i<bytes.length; i++) {
			value = (int) bytes[i] & 0x000000FF; 
			sb.append( Integer.toHexString(value) );
		}
		
		Log.d(tag, sb.toString());
	}
}
